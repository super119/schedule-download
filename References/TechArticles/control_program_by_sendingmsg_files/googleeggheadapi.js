var loader;
var web;
var googleSearch;
 
function OnEggHeadCafeSearchLoad() 
{
  web = new EggHeadCafeSearchLinks('googlesearchdiv');
 // GSearch.getBranding(document.getElementById("brand"));
} 
 
function EggHeadCafeSearchLinks(search){ this.buildSearchControl(search); }
 
EggHeadCafeSearchLinks.prototype.buildSearchControl = function(search)
{
  googleSearch = new GwebSearch(); 
  googleSearch.name=search;
  googleSearch.setResultSetSize(GSearch.LARGE_RESULTSET);
  googleSearch.setNoHtmlGeneration(); 
  googleSearch.setSearchCompleteCallback(this, EggHeadCafeSearchLinks.prototype.searchComplete, [null]);
}
 
EggHeadCafeSearchLinks.prototype.execute = function(query){ googleSearch.execute(query);}
 
EggHeadCafeSearchLinks.prototype.clearAllResults = function () { googleSearch.clearAllResults; }

 
EggHeadCafeSearchLinks.prototype.searchComplete = function ()
{
  resultarr= googleSearch.results;
  var cursor = googleSearch.cursor;
  var panel=document.getElementById(googleSearch.name);
  resTable=document.createElement('div');
 
  if (resultarr.length==0) 
  {
      resTable.style.padding="10px";
      resTable.innerHTML='Query did not match any documents.';
      panel.appendChild(resTable);  
      return;
  }
  	  
  for (i=0; i < resultarr.length; i++ )
  { 
	    rcont=document.createElement('div');
 	    rcont.className='gs-result';
	    if ((i%2)==0) rcont.style.background='#EBFFFF';
 
	    title=document.createElement('div');
	    LINK=document.createElement('a');
		LINK.setAttribute('target','_blank');
	    LINK.className= 'gs-title';
	    LINK.href=resultarr[i].unescapedUrl;
		LINK.innerHTML=resultarr[i].title;
        title.appendChild(LINK);
            
        desc=document.createElement('div');
        
		with (desc)
		{
	    	 className= 'gs-text-box';
	         innerHTML=resultarr[i].content;
		}
          	   
	    rcont.appendChild(title); 					
	    rcont.appendChild(desc);
        resTable.appendChild(rcont);	                  
  }
  
   pageLinks=document.createElement('div');
   var selected = '';
   
  for(i=0;i<cursor.pages.length;i++)
  {
  
    selected = '';
    
    if (i==cursor.currentPageIndex)
    {
      selected = 'selected';
    }
    
    LINK=document.createElement('a');
    LINK.setAttribute('target','_self');
    LINK.href='javascript:GotoGoogleSearchResultPage(' + (cursor.pages[i].label -1) + ')';
    LINK.innerHTML='<img src="/graphics/spheres/Sphere' + cursor.pages[i].label + selected +'.png" alt="' + cursor.pages[i].label + '">';
    pageLinks.appendChild(LINK);
 
  }
   
  resTable.appendChild(pageLinks);	
  panel.appendChild(resTable);  
		
}

function GotoGoogleSearchResultPage(pageNumber)
{
  document.getElementById('googlesearchdiv').innerHTML="";
  googleSearch.gotoPage(pageNumber);
}
 

function LoadGoogleSearchResults(searchTerm)
{
  document.getElementById('googlesearchdiv').innerHTML="";
  loader=0;
  web.clearAllResults();
  web.execute(searchTerm);     
}
  
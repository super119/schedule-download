/*
 * RAS Test application
 * Written by Eric Zhang
 */

#include <windows.h>
#include <tchar.h>
#include <Ras.h>
#include <RasError.h>
#include <strsafe.h>

#define OPERATION_SUCCESS	0
#define	OPERATION_FAILED	1

typedef DWORD (APIENTRY *enum_entries)(LPCTSTR, LPCTSTR, LPRASENTRYNAME, LPDWORD, LPDWORD);
typedef DWORD (APIENTRY *get_entry_dial_params)(LPCTSTR, LPRASDIALPARAMS, LPBOOL);

int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
				     LPTSTR lpCmdLine, int nShowCmd)
{
	HMODULE ras_module = NULL;
	enum_entries ras_enum_entries;
	get_entry_dial_params ras_get_entry_dial_params;
	RASENTRYNAME ras_entry;
	RASENTRYNAME *all_entries = NULL;
	RASDIALPARAMS ras_dial_params;
	DWORD total_bytes;
	DWORD total_entries;
	DWORD result;
	TCHAR string_buffer[255];
	BOOL save_password = FALSE;

	// load rasapi32.dll
	ras_module = LoadLibrary(TEXT("rasapi32.dll"));
	if (ras_module == NULL) {
		MessageBox(NULL, TEXT("Load rasapi32.dll failed. Quit."), TEXT("Error"), MB_ICONERROR | MB_OK);
		goto failed;
	}
	
	// get function address
	ras_enum_entries = (enum_entries)GetProcAddress(ras_module, "RasEnumEntriesW");
	if (ras_enum_entries == NULL) {
		MessageBox(NULL, TEXT("Get RasEnumEntries func failed, quit."), TEXT("Error"), MB_OK | MB_ICONERROR);
		goto failed;
	}
	ras_get_entry_dial_params = (get_entry_dial_params)GetProcAddress(ras_module, "RasGetEntryDialParamsW");
	if (ras_get_entry_dial_params == NULL) {
		MessageBox(NULL, TEXT("Get RasGetEntryDialParams func failed, quit."), TEXT("Error"), MB_OK | MB_ICONERROR);
		goto failed;
	}

	// enum ras entries
	ras_entry.dwSize = sizeof(RASENTRYNAME);
	total_bytes = sizeof(RASENTRYNAME);
	result = ras_enum_entries(NULL, NULL, &ras_entry, &total_bytes, &total_entries);
	if (result == 0) {
		// only one entry, display it
		ras_dial_params.dwSize = sizeof(RASDIALPARAMS);
		StringCchCopy(ras_dial_params.szEntryName, _countof(ras_dial_params.szEntryName), ras_entry.szEntryName);
		ras_get_entry_dial_params(ras_entry.szPhonebookPath, &ras_dial_params, &save_password);
		StringCchPrintf(string_buffer, _countof(string_buffer), TEXT("Entry name: %s, Flag: %d, Phone Book Path: %s, Save password? %d"), 
			ras_entry.szEntryName, ras_entry.dwFlags, ras_entry.szPhonebookPath, save_password);
		MessageBox(NULL, string_buffer, TEXT("Success"), MB_OK | MB_ICONINFORMATION);
	} else if (result == ERROR_BUFFER_TOO_SMALL) {
		// malloc more storage to store all entries
		all_entries = (RASENTRYNAME *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, total_bytes);
		all_entries[0].dwSize = sizeof(RASENTRYNAME);
		// call again
		result = ras_enum_entries(NULL, NULL, all_entries, &total_bytes, &total_entries);
		if (result == 0) {
			// success, display
			for (unsigned int i = 0; i < total_entries; i++) {
				ras_dial_params.dwSize = sizeof(RASDIALPARAMS);
				StringCchCopy(ras_dial_params.szEntryName, _countof(ras_dial_params.szEntryName), all_entries[i].szEntryName);
				ras_get_entry_dial_params(all_entries[i].szPhonebookPath, &ras_dial_params, &save_password);
				StringCchPrintf(string_buffer, _countof(string_buffer), TEXT("Entry name: %s, Flag: %d, Phone Book Path: %s, Save password? %d"), 
					all_entries[i].szEntryName, all_entries[i].dwFlags, all_entries[i].szPhonebookPath, save_password);
				MessageBox(NULL, string_buffer, TEXT("Success"), MB_OK | MB_ICONINFORMATION);
			}
		} else {
			MessageBox(NULL, TEXT("Error calling RasEnumEntries."), TEXT("Error"), MB_OK | MB_ICONERROR);
		}
	}


	if (all_entries != NULL) HeapFree(GetProcessHeap(), NULL, all_entries);
	if (ras_module != NULL) FreeLibrary(ras_module);
	return OPERATION_SUCCESS;

failed:
	if (all_entries != NULL) HeapFree(GetProcessHeap(), NULL, all_entries);
	if (ras_module != NULL) FreeLibrary(ras_module);
	return OPERATION_FAILED;
}
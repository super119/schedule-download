#include <windows.h>
#include <tchar.h>
#include <locale.h>

int _tmain(int argc, TCHAR *argv[], TCHAR *envp[])
{
	HKEY key;
	TCHAR databuf[100];
	DWORD buflen = sizeof(databuf);
	LONG lRet;

	// Set locale to make chinese displays
	setlocale(LC_ALL, "CHS"); 

	// open a registry key
	lRet = RegOpenKeyEx(HKEY_CURRENT_USER, TEXT("Software\\360Safe\\traynotify"), 
						0, KEY_QUERY_VALUE, &key);
	if (lRet != ERROR_SUCCESS) {
		HLOCAL hLocal = NULL;  // buffer that gets the error msg string
		DWORD systemLocale = MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL);
		BOOL bOK = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER, 
					NULL, lRet, systemLocale, (PTSTR)&hLocal, 0, NULL);
		if (bOK) {
			_tprintf(TEXT("Open HKEY_CURRENT_USER\\Software\\360Safe\\traynotify failed. Reason: %s\n"), 
					 (PCTSTR)LocalLock(hLocal));
		} else {
			_tprintf(TEXT("Open HKEY_CURRENT_USER\\Software\\360Safe\\traynotify failed.\n"));
		}
		if (hLocal != NULL)
			LocalFree(hLocal);

		return 1;
	}

	// query the key value
	_tprintf(TEXT("Size of databuf is %d\n"), buflen);
	lRet = RegQueryValueEx(key, TEXT("SosScanDate"), NULL, NULL, (LPBYTE)databuf, &buflen);
	if (lRet != ERROR_SUCCESS || buflen > sizeof(databuf)) {
		HLOCAL hLocal = NULL;  // buffer that gets the error msg string
		DWORD systemLocale = MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL);
		BOOL bOK = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER, 
					NULL, lRet, systemLocale, (PTSTR)&hLocal, 0, NULL);
		if (bOK) {
			_tprintf(TEXT("Query HKEY_CURRENT_USER\\Software\\360Safe\\traynotify\\SosScanDate failed. Reason: %s\n"), 
					 (PCTSTR)LocalLock(hLocal));
		} else {
			_tprintf(TEXT("Query HKEY_CURRENT_USER\\Software\\360Safe\\traynotify\\SosScanDate failed.\n"));
		}
		if (hLocal != NULL)
			LocalFree(hLocal);

		return 1;
	}

	// print out result
	_tprintf(TEXT("Got HKEY_CURRENT_USER\\Software\\360Safe\\traynotify\\SosScanDate: %s"), databuf);

	RegCloseKey(key);
	return 0;

}
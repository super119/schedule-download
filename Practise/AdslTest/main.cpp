/*
 * SDCommon ADSL Module test program
 */

#include <sdcommon.h>
#include <conio.h>
#include <locale.h>

int _tmain(int argc, TCHAR *argv[], TCHAR *env[])
{
	TCHAR root_dir[MAX_PATH + 1];
	RASENTRYNAME *entry_list = NULL;
	HRASCONN ras_conn = NULL;
	DWORD entry_num;
	BOOL result = FALSE;

	setlocale(LC_ALL, "CHS");

	// root directory
	result = sdc_utils_find_root_directory(root_dir);
	if (!result) {
		_tprintf(TEXT("Find out root directory failed.\n"));
		goto failed;
	}

	// init logger
	result = sdc_logger_enable(root_dir, TEXT("adsltest.log"));
	if (!result) {
		_tprintf(TEXT("Init logger failed.\n"));
		goto failed;
	}

	// enum current ras entries
	entry_list = sdc_adsl_enum_entries(&entry_num);
	if (entry_list == NULL) {
		if (entry_num >= 0) {
			_tprintf(TEXT("No RAS Entry found in your machine.\n"));
			goto failed;
		} else {
			_tprintf(TEXT("Enum RAS entries failed. Check the log file to get the reason.\n"));
			goto failed;
		}
	} else {
		// print entry list
		for (DWORD i=0; i<entry_num; i++) {
			_tprintf(TEXT("Print %d entry. Phonebook path: %s, Entry name: %s\n"), i, entry_list->szPhonebookPath, entry_list->szEntryName);
		}
	}

	// dial ADSL
	_tprintf(TEXT("\n"));
	_tprintf(TEXT("Trying to dial entry 宽带连接...\n"));
	result = sdc_adsl_dial(TEXT("C:\\Documents and Settings\\All Users\\Application Data\\Microsoft\\Network\\Connections\\Pbk\\rasphone.pbk"), 
		TEXT("宽带连接"), &ras_conn);
	if (!result || !ras_conn) {
		_tprintf(TEXT("Dial entry 宽带连接 failed. Check the log file for reasons.\n"));
		goto failed;
	}

	_getch();

	// Hangup the connection
	_tprintf(TEXT("\n"));
	_tprintf(TEXT("Hangup the connection 宽带连接...\n"));
	result = sdc_adsl_hangup(ras_conn);
	if (!result) {
		_tprintf(TEXT("Hangup 宽带连接 failed. Check the log file for reasons.\n"));
		goto failed;
	}

	_getch();
	return 0;
failed:
	_getch();
	return 1;
}
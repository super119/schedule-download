#include <windows.h>
#include <tchar.h>
#include <conio.h>
#include <map>
#include <locale.h>
#include "common.h"
#include "add.h"
using namespace std;

TextResMap map1;

int _tmain(int argc, TCHAR *argv[], TCHAR *envp[])
{
	setlocale(LC_ALL, "CHS");

	insert_stuffs();

	TCHAR *tmp_key1 = TEXT("��������è");
	TCHAR *tmp_key2 = TEXT("���ķ籩");

	// check out the key/value
	TextResMap::iterator it = map1.find(tmp_key1);
	if (it != map1.end())
		_tprintf(TEXT("Found value for key %s is %s\n"), tmp_key1, it->second);
	else
		_tprintf(TEXT("No entry found in the map.\n"));

	it = map1.find(tmp_key2);
	if (it != map1.end())
		_tprintf(TEXT("Found value for key %s is %s\n"), tmp_key2, it->second);
	else
		_tprintf(TEXT("No entry found in the map.\n"));

	// wait until key pressed
	_getch();

	release_resource();

	return 0;
}
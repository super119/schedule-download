#include <tchar.h>
#include <map>
using namespace std;

struct cmp_str
{
	bool operator()(const TCHAR *a, const TCHAR *b)
	{
		return _tcscmp(a, b) < 0;
	}
};

typedef map<TCHAR *, TCHAR *, cmp_str>	TextResMap;
// typedef map<TCHAR *, TCHAR *>	TextResMap;
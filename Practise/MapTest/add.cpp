#include <map>
#include <windows.h>
#include <tchar.h>
#include "common.h"
#include <strsafe.h>

extern TextResMap map1;

TCHAR *key1 = NULL;
TCHAR *key2 = NULL;
TCHAR *value1 = NULL;
TCHAR *value2 = NULL;

void insert_stuffs()
{
	HANDLE hHeap = GetProcessHeap();
	// just use 100 bytes, enough here
	key1 = (TCHAR *)HeapAlloc(hHeap, HEAP_ZERO_MEMORY, 100);
	key2 = (TCHAR *)HeapAlloc(hHeap, HEAP_ZERO_MEMORY, 100);
	value1 = (TCHAR *)HeapAlloc(hHeap, HEAP_ZERO_MEMORY, 100);
	value2 = (TCHAR *)HeapAlloc(hHeap, HEAP_ZERO_MEMORY, 100);

	StringCchPrintf(key1, 100 / sizeof(TCHAR), TEXT("%s"), TEXT("��������è"));
	StringCchPrintf(key2, 100 / sizeof(TCHAR), TEXT("%s"), TEXT("���ķ籩"));
	StringCchPrintf(value1, 100 / sizeof(TCHAR), TEXT("%s"), TEXT("�޼���/���Ӷ�"));
	StringCchPrintf(value2, 100 / sizeof(TCHAR), TEXT("%s"), TEXT("�º�/���ٷ�"));

	// insert some stuffs into it
	map1.insert(make_pair(key1, value1));
	map1.insert(make_pair(key2, value2));
}

void release_resource()
{
	HANDLE hHeap = GetProcessHeap();
	HeapFree(hHeap, NULL, key1);
	HeapFree(hHeap, NULL, key2);
	HeapFree(hHeap, NULL, value1);
	HeapFree(hHeap, NULL, value2);
}
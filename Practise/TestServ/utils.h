/*
 * Schedule Download Utilities
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#ifndef _UTILS_H
#define _UTILS_H

#include <windows.h>
#include <tchar.h>
#include <process.h>
#include "logger.h"

#define __SDFILE__		TEXT(__FILE__)

#define GET_ERROR_CODE(n)	\
	n = GetLastError();

#define UTILS_RETURN_IF_FAIL(expr)	\
	if (!(expr)) return;

#define UTILS_RETURN_VAL_IF_FAIL(expr, val) \
	if (!(expr)) return val;

#define UTILS_GOTO_IF_FAIL(expr, label)	\
	if (!(expr)) goto label;

#define UTILS_CONT_IF_FAIL(expr)	\
	if (!(expr)) continue;

#define UTILS_RIF_WITH_LOG(expr, log_level, filename, line, format, ...)	\
	if (!(expr)) {	\
		LOGGER(log_level, filename, line, format, __VA_ARGS__);	\
		return;	\
	}

#define UTILS_RVIF_WITH_LOG(expr, val, log_level, filename, line, format, ...)	\
	if (!(expr)) {	\
		LOGGER(log_level, filename, line, format, __VA_ARGS__);	\
		return val;	\
	}

#define UTILS_GTIF_WITH_LOG(expr, label, log_level, filename, line, format, ...)	\
	if (!(expr)) {	\
		LOGGER(log_level, filename, line, format, __VA_ARGS__);	\
		goto label;	\
	}

typedef unsigned (__stdcall *PTHREAD_START) (void *);
#define SDBEGINTHREADEX(psa, cbStackSize, pfnStartAddr, pvParam, fdwCreateFlags, pdwThreadID)	\
      ((HANDLE) _beginthreadex(                     \
         (void *) (psa),                            \
         (unsigned) (cbStackSize),                  \
         (PTHREAD_START) (pfnStartAddr),            \
         (void *) (pvParam),                        \
         (unsigned) (fdwCreateFlags),                \
         (unsigned *) (pdwThreadID)))

/* 
 * Convert system error code to error message string
 * Caller should call 'LocalFree' to release the string memory
 */
PTSTR utils_format_error_string(const LONG code);

/*
 * See whether a file exists by calling GetFileAttributes
 */
BOOL utils_file_exists(PCTSTR file_path);

/*
 * Trim the leading spaces in the string
 */
TCHAR *utils_trim_left(TCHAR *input);

/*
 * Trim the trailing spaces in the string
 */
TCHAR *utils_trim_right(TCHAR *input);

/*
 * Trim the left & right spaces of string
 */
TCHAR *utils_trim_both(TCHAR *input);

/*
 * Remove the trailing enter(\r)
 */
TCHAR *utils_remove_trailing_enter(TCHAR *input);

#endif
/*
 * Schedule Download Utilities
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#include <windows.h>
#include "utils.h"
#include <strsafe.h>

PTSTR utils_format_error_string(const LONG code)
{
	HLOCAL hLocal = NULL;  // Buffer to get the error msg string
	DWORD systemLocale;
	BOOL ret;

	if (code < 0) {
		// illegal error code
		return NULL;
	}

	systemLocale = MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL);
	ret = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER, 
						NULL, code, systemLocale, (PTSTR)&hLocal, 0, NULL);
	if (ret) {
		return (PTSTR)hLocal;			
	}
	else {
		return NULL;
	}
}

BOOL utils_file_exists(PCTSTR file_path)
{
	DWORD file_attr;

	UTILS_RETURN_VAL_IF_FAIL(file_path != NULL, FALSE);
	file_attr = GetFileAttributes(file_path);
	if (file_attr == INVALID_FILE_ATTRIBUTES) return FALSE;
	if (file_attr & FILE_ATTRIBUTE_DIRECTORY) return FALSE;

	return TRUE;
}

TCHAR *utils_trim_left(TCHAR *input)
{
	UTILS_RETURN_VAL_IF_FAIL(input != NULL, NULL);

	while ((*input) == (TCHAR)' ')
		input++;

	return input;
}

TCHAR *utils_trim_right(TCHAR *input)
{
	size_t input_string_len = 0;
	TCHAR *tmp = NULL;

	UTILS_RETURN_VAL_IF_FAIL(input != NULL, NULL);
	UTILS_RETURN_VAL_IF_FAIL(SUCCEEDED(StringCchLength(input, STRSAFE_MAX_CCH, &input_string_len)), NULL);
	tmp = input + input_string_len - 1;
	
	while ((*tmp) == (TCHAR)' ') {
		*tmp = (TCHAR)'\0';
		tmp--;
	}

	// we just return the original pointer because we only modifies
	// the trailing part of the string
	// we defined a return value for user check whether the function succeeded
	return input;
}

TCHAR *utils_trim_both(TCHAR *input)
{
	TCHAR *tmp = NULL;
	UTILS_RETURN_VAL_IF_FAIL(input != NULL, NULL);

	tmp = utils_trim_left(input);
	UTILS_RETURN_VAL_IF_FAIL(tmp != NULL, NULL);

	tmp = utils_trim_right(tmp);
	UTILS_RETURN_VAL_IF_FAIL(tmp != NULL, NULL);

	return tmp;
}

TCHAR *utils_remove_trailing_enter(TCHAR *input)
{
	size_t input_string_len = 0;
	TCHAR *tmp = NULL;

	UTILS_RETURN_VAL_IF_FAIL(input != NULL, NULL);
	UTILS_RETURN_VAL_IF_FAIL(SUCCEEDED(StringCchLength(input, STRSAFE_MAX_CCH, &input_string_len)), NULL);
	tmp = input + input_string_len - 1;
	
	while ((*tmp) == (TCHAR)'\r') {
		*tmp = (TCHAR)'\0';
		tmp--;
	}

	// we just return the original pointer because we only modifies
	// the trailing part of the string
	// we defined a return value for user check whether the function succeeded
	return input;
}

/*
 * Schedule Download logger
 * The logger writes string into file "sdclient.log"
 * in program root directory
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#include "logger.h"
#include "utils.h"
#include <strsafe.h>

#define LOG_STRING_MAX		4096
#define MEGA_BYTES			1024 * 1024
#define MAX_LOG_ROTATE_NUM	10

// log directory path
TCHAR log_dir_path[MAX_PATH];

BOOL logger_set_root_directory(PCTSTR root_dir)
{
	size_t root_dir_len = 0;
	UTILS_RETURN_VAL_IF_FAIL(root_dir != NULL, FALSE);

	// generate log dir path now
	UTILS_RETURN_VAL_IF_FAIL(SUCCEEDED(StringCchLength(root_dir, MAX_PATH, &root_dir_len)), FALSE);
	if (root_dir[root_dir_len - 1] == (TCHAR)'\\') {
		if (FAILED(StringCchPrintf(log_dir_path, _countof(log_dir_path), TEXT("%s%s"), root_dir, TEXT("log")))) {
			// use root_directory as log directory directly
			UTILS_RETURN_VAL_IF_FAIL(SUCCEEDED(StringCchCopy(log_dir_path, _countof(log_dir_path), root_dir)), FALSE);
		}
	} else {
		if (FAILED(StringCchPrintf(log_dir_path, _countof(log_dir_path), TEXT("%s%s"), root_dir, TEXT("\\log")))) {
			// use root_directory as log directory directly
			UTILS_RETURN_VAL_IF_FAIL(SUCCEEDED(StringCchCopy(log_dir_path, _countof(log_dir_path), root_dir)), FALSE);
		}
	}

	return TRUE;
}

static BOOL prepare_log_rotate(PCTSTR log_file_path)
{
	TCHAR temp_buffer[MAX_PATH];
	HANDLE hFile = NULL;
	HANDLE hFind = NULL;
	WIN32_FIND_DATA wfd;
	FILETIME earliest_file_time;
	int cur_rotate_file_number = 0;
	BOOL cur_rotate_file_exist = TRUE;
	LARGE_INTEGER file_size;
	BOOL operate_result = FALSE;
	int i = 0;
	DWORD bytes_written = 0;

	UTILS_RETURN_VAL_IF_FAIL(log_file_path != NULL, FALSE);

	// OK, first we check out "logfile"
	if (!utils_file_exists(log_file_path)) {
		// logfile doesn't exist, create it, that's all
		hFile = CreateFile(log_file_path, GENERIC_WRITE, NULL, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile == INVALID_HANDLE_VALUE) {
			operate_result = FALSE;
		} else {
			// write 0xfffe at the beginning of the file, this makes Notepad reads Unicodes well
			WORD unicode_identifier = 0xfeff;
			if (WriteFile(hFile, &unicode_identifier, sizeof(WORD), &bytes_written, NULL)) {
				operate_result = TRUE;
			} else {
				operate_result = FALSE;
			}
		}
		goto finished;
	} else {
		// "logfile" exists, check it's size
		hFile = CreateFile(log_file_path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile == INVALID_HANDLE_VALUE) {
			operate_result = FALSE;
			goto finished;
		}
		if (!GetFileSizeEx(hFile, &file_size)) {
			operate_result = FALSE;
			goto finished;
		}

		if (file_size.QuadPart > MEGA_BYTES) {
			// rename "logfile" to an archive file and continue
			// first, which archive file we can use now?
			// if logfile.0 -- logfile.MAX_LOG_ROTATE_NUM doesn't all exist, we use the left
			// if logfile.0 -- logfile.MAX_LOGROTATE_NUM all exist, we use the earliest by file timestamp
			for (int i=0; i < MAX_LOG_ROTATE_NUM; i++) {
				if (FAILED(StringCchPrintf(temp_buffer, _countof(temp_buffer), TEXT("%s%s%d"), log_file_path, TEXT("."), i)))
					continue;
				hFind = FindFirstFile(temp_buffer, &wfd);
				if (hFind == INVALID_HANDLE_VALUE) {
					// file not found, break
					cur_rotate_file_number = i;
					cur_rotate_file_exist = FALSE;
					break;
				} else {
					// check out the file's last write time
					if (i == 0) {
						memcpy_s(&earliest_file_time, sizeof(FILETIME), &(wfd.ftLastWriteTime), sizeof(FILETIME));
					} else {
						if (CompareFileTime(&earliest_file_time, &(wfd.ftLastWriteTime))) {
							memcpy_s(&earliest_file_time, sizeof(FILETIME), &(wfd.ftLastWriteTime), sizeof(FILETIME));
							cur_rotate_file_number = i;
						}
					}
					FindClose(hFind);
				}
			}

			// OK, remove current rotate file and rename "logfile" to current rotate file
			UTILS_GOTO_IF_FAIL(SUCCEEDED(StringCchPrintf(temp_buffer, _countof(temp_buffer), TEXT("%s%s%d"), log_file_path, TEXT("."), cur_rotate_file_number)), finished);
			if (cur_rotate_file_exist) {
				if (!DeleteFile(temp_buffer)) {
					DWORD error_num = GetLastError();
					if (error_num == ERROR_ACCESS_DENIED) {
						// remove file's readonly attribute
						SetFileAttributes(temp_buffer, FILE_ATTRIBUTE_NORMAL);
						// try delete again.
						DeleteFile(temp_buffer);
					}
				}
			}
			
			// Rename current "logfile". Close the handle first
			CloseHandle(hFile);
			hFile = NULL;
			MoveFile(log_file_path, temp_buffer);

			// OK, now we prepare an empty "logfile", then done.
			hFile = CreateFile(log_file_path, GENERIC_WRITE, NULL, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
			if (hFile == INVALID_HANDLE_VALUE) {
				operate_result = FALSE;
			} else {
				// write 0xfffe at the beginning of the file, this makes Notepad reads Unicodes well
				WORD unicode_identifier = 0xfeff;
				if (WriteFile(hFile, &unicode_identifier, sizeof(WORD), &bytes_written, NULL)) {
					operate_result = TRUE;
				} else {
					operate_result = FALSE;
				}
			}
			goto finished;
		} else {
			// "logfile" exists and filesize is OK. So nothing should be done here
			operate_result = TRUE;
			goto finished;
		}
	}	
finished:
	if (hFile != NULL && hFile != INVALID_HANDLE_VALUE)
		CloseHandle(hFile);
	return operate_result;
}

void logger_action(LOG_LEVEL log_level, LPCTSTR filename, int line, LPCTSTR format, ...)
{
	TCHAR log_str_buf[LOG_STRING_MAX];
	size_t log_str_cur_index = 0;
	SYSTEMTIME cur_time;
	TCHAR log_file_path[MAX_PATH];
	HANDLE hFile = NULL;
	LARGE_INTEGER file_size;
	DWORD chars_need_write;
	DWORD bytes_written;

	UTILS_RETURN_IF_FAIL(filename != NULL);
	UTILS_RETURN_IF_FAIL(format != NULL);

	// get date and time
	GetLocalTime(&cur_time);

	// create log directory whatever
	// we don't care about whether log directory exists
	CreateDirectory(log_dir_path, NULL);

	// we use log rotate facility -- at most keep 11 log files
	// each file has a 1M size. logfile, logfile.0, logfile.1 ... logfile.MAX_LOG_ROTATE_NUM
	// we always write to "logfile" but we should do log roate first 
	UTILS_RETURN_IF_FAIL(SUCCEEDED(StringCchPrintf(log_file_path, _countof(log_file_path), TEXT("%s%s"), log_dir_path, TEXT("\\logfile"))));
	UTILS_RETURN_IF_FAIL(prepare_log_rotate(log_file_path));

	// begin create log string
	switch(log_level)
	{
	case LOG_LEVEL_INFO:
		UTILS_RETURN_IF_FAIL(SUCCEEDED(StringCchPrintf(log_str_buf, _countof(log_str_buf), TEXT("INFO|%s:%d|%d-%d-%d %d:%d:%d|"), 
			filename, line, cur_time.wYear, cur_time.wMonth, cur_time.wDay, cur_time.wHour, cur_time.wMinute, cur_time.wSecond)));
		break;
	case LOG_LEVEL_WARNING:
		UTILS_RETURN_IF_FAIL(SUCCEEDED(StringCchPrintf(log_str_buf, _countof(log_str_buf), TEXT("WARN|%s:%d|%d-%d-%d %d:%d:%d|"), 
			filename, line, cur_time.wYear, cur_time.wMonth, cur_time.wDay, cur_time.wHour, cur_time.wMinute, cur_time.wSecond)));
		break;
	case LOG_LEVEL_ERROR:
		UTILS_RETURN_IF_FAIL(SUCCEEDED(StringCchPrintf(log_str_buf, _countof(log_str_buf), TEXT("ERROR|%s:%d|%d-%d-%d %d:%d:%d|"), 
			filename, line, cur_time.wYear, cur_time.wMonth, cur_time.wDay, cur_time.wHour, cur_time.wMinute, cur_time.wSecond)));
		break;
	}

	// get current log buffer position
	UTILS_RETURN_IF_FAIL(SUCCEEDED(StringCchLength(log_str_buf, _countof(log_str_buf), &log_str_cur_index)));

	// handle format & args
	va_list args;
    va_start(args, format);
	UTILS_RETURN_IF_FAIL(SUCCEEDED(StringCchVPrintf(log_str_buf + log_str_cur_index, 
						_countof(log_str_buf) - log_str_cur_index, format, args)));
	va_end(args);

	// add a '\n' to the log string
	UTILS_RETURN_IF_FAIL(SUCCEEDED(StringCchLength(log_str_buf, _countof(log_str_buf), &log_str_cur_index)));
	UTILS_RETURN_IF_FAIL(SUCCEEDED(StringCchCat(log_str_buf + log_str_cur_index, 
						_countof(log_str_buf) - log_str_cur_index, TEXT("\n"))));

	// OK, begin logging
	hFile = CreateFile(log_file_path, GENERIC_WRITE, NULL, NULL, OPEN_EXISTING, 
					FILE_ATTRIBUTE_NORMAL | FILE_FLAG_RANDOM_ACCESS, NULL);
	UTILS_GOTO_IF_FAIL(hFile != INVALID_HANDLE_VALUE, failed);

	// Seek to the end of file
	UTILS_GOTO_IF_FAIL(GetFileSizeEx(hFile, &file_size), failed);
	UTILS_GOTO_IF_FAIL(SetFilePointerEx(hFile, file_size, NULL, FILE_BEGIN), failed);

	// append data
	UTILS_GOTO_IF_FAIL(SUCCEEDED(StringCchLength(log_str_buf, LOG_STRING_MAX, (size_t *)&chars_need_write)), failed);
	if (WriteFile(hFile, log_str_buf, chars_need_write * sizeof(TCHAR), &bytes_written, NULL)) {
		CloseHandle(hFile);
		hFile = NULL;
	} else {
		goto failed;
	}

	return;

failed:
	if (hFile != NULL && hFile != INVALID_HANDLE_VALUE)
		CloseHandle(hFile);
}

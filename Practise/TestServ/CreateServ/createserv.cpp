#include <windows.h>
#include <tchar.h>
#include "utils.h"
#include <locale.h>
#include <conio.h>
#include <strsafe.h>

#define SVCNAME TEXT("EricTestService")

int _tmain(int argc, PTSTR argv[], PTSTR env[])
{
	DWORD errcode = 0;
	SC_HANDLE hManager = NULL;
	SC_HANDLE hService = NULL;

	setlocale(LC_ALL, "CHS");

	// Open SCManager
	hManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if (NULL == hManager) {
		errcode = GetLastError();
		_tprintf(TEXT("Open service manager failed. Reason: %s\n"), utils_format_error_string(errcode));
		goto failed;
	}

	// create service
	hService = CreateService( 
        hManager,	               // SCM database 
        SVCNAME,                   // name of service 
        SVCNAME,                   // service name to display 
        SERVICE_ALL_ACCESS,        // desired access 
        SERVICE_WIN32_OWN_PROCESS, // service type 
        SERVICE_AUTO_START,      // start type 
        SERVICE_ERROR_NORMAL,      // error control type 
		TEXT("C:\\ScheduleDownload\\Practise\\TestServ\\Debug\\TestServ.exe"),  // path to service's binary 
        NULL,                      // no load ordering group 
        NULL,                      // no tag identifier 
        NULL,                      // no dependencies 
        NULL,                      // LocalSystem account 
        NULL);                     // no password

	if (NULL == hService) {
		errcode = GetLastError();
		_tprintf(TEXT("Create service failed. Reason: %s\n"), utils_format_error_string(errcode));
		CloseServiceHandle(hManager);
		goto failed;
	}

	CloseServiceHandle(hService);
	CloseServiceHandle(hManager);

	return 0;

failed:
	// wait key
	_getch();
	return 1;
}
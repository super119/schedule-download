#include "utils.h"

PTSTR utils_format_error_string(const LONG code)
{
	HLOCAL hLocal = NULL;  // Buffer to get the error msg string
	DWORD systemLocale;
	BOOL ret;

	if (code < 0) {
		// illegal error code
		return NULL;
	}

	systemLocale = MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL);
	ret = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER, 
						NULL, code, systemLocale, (PTSTR)&hLocal, 0, NULL);
	if (ret) {
		return (PTSTR)hLocal;			
	}
	else {
		return NULL;
	}
}
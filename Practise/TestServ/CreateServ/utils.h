#ifndef _UTILS_H
#define _UTILS_H

#include <windows.h>
#include <tchar.h>

/* 
 * Convert system error code to error message string
 * Caller should call 'LocalFree' to release the string memory
 */
PTSTR utils_format_error_string(const LONG code);


#endif
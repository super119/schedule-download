/*
 * Schedule Download logger
 * The logger writes string into file "sdclient.log"
 * in program root directory
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#ifndef _LOGGER_H
#define _LOGGER_H

#include <windows.h>
#include <tchar.h>

enum LOG_LEVEL
{
	LOG_LEVEL_INFO, 
	LOG_LEVEL_WARNING,
	LOG_LEVEL_ERROR
};

#ifdef _DEBUG
#define LOGGER(log_level, filename, line, format, ...)	\
	logger_action(log_level, filename, line, format, __VA_ARGS__);
#else
#define LOGGER
#endif

/*
 * Set program root directory. So logger will create a "log" directory in it
 * and place log files in it
 * CAUTION: call this function makes logger available.
 */
BOOL logger_set_root_directory(PCTSTR root_dir);

/*
 * Log the strings. filename should be __FILE__ and line should be __LINE__
 */
void logger_action(LOG_LEVEL log_level, LPCTSTR filename, int line, LPCTSTR format, ...);


#endif
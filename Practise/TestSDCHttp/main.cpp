/*
 * Test SDCommon http module
 */

#include <sdcommon.h>
#include <conio.h>
#include <stdio.h>

int _tmain(int argc, TCHAR *argv[], TCHAR *env[])
{
	HINTERNET session, connect, request;
	BOOL result = FALSE;
	TCHAR root_dir[MAX_PATH];
	char buffer[4097];   // 4K buffer, allocate 1 extra byte to save '\0'
	DWORD data_len = 0;
	DWORD bytes_read = 0;
	DWORD bytes_written = 0;
	HANDLE hFile = NULL;

	// find out root directory first
	result = sdc_utils_find_root_directory(root_dir);
	if (!result) {
		_tprintf(TEXT("Find out root directory failed, quit...\n"));
		goto failed;
	}

	// init logger
	result = sdc_logger_enable(root_dir, TEXT("testsdchttp.log"));
	if (!result) {
		_tprintf(TEXT("Init logger failed, quit...\n"));
		goto failed;
	}

	// check internet connection
	result = sdc_http_check_internet(TEXT("http://www.g.cn/"));
	if (result) {
		_tprintf(TEXT("Internet is available now...\n"));
	} else {
		_tprintf(TEXT("Internet is NOT available now...\n"));
		goto failed;
	}

	_tprintf(TEXT("Trying to get text http data...\n"));
	// get http data
	session = connect = request = NULL;
	result = sdc_http_get_text(TEXT("http://bbs.archlinux.org/viewtopic.php?id=56373&add1=hello&add2=hello2#first"), &session, &connect, &request);
	if (!result) {
		_tprintf(TEXT("Call sdc_http_get_text failed...\n"));
		goto failed;
	}

	while (sdc_http_check_data_len(request, &data_len) && data_len > 0) {
		result = sdc_http_recv(request, buffer, 4096, &bytes_read);
		if (!result) {
			_tprintf(TEXT("Read data failed, quit...\n"));
			goto failed;
		}

		if (bytes_read < 4096) {
			// seems no more data available
			buffer[bytes_read] = '\0';
		} else {
			buffer[4096] = '\0';
		}
		// print out buffer
		printf("%s", buffer);
	}

	_getch();
	sdc_http_close(&session, &connect, &request);

	_tprintf(TEXT("\n"));
	_tprintf(TEXT("Trying to get binary http data...\n"));

	session = connect = request = NULL;
	// result = sdc_http_get_bin(TEXT("http://p.greedland.net/download.php?t=2009/05/14/1242274834.torrent"), &session, &connect, &request);
	result = sdc_http_get_bin(TEXT("http://gl.greedland.net/UploadFile/2008/9/200899164711.jpg"), &session, &connect, &request);
	if (!result) {
		_tprintf(TEXT("Call sdc_http_get_bin failed...\n"));
		goto failed;
	}

	// open file for write
	hFile = CreateFile(TEXT("file.down"), GENERIC_WRITE, NULL, NULL, CREATE_ALWAYS, 
					FILE_ATTRIBUTE_NORMAL | FILE_FLAG_RANDOM_ACCESS, NULL);
	if (hFile == INVALID_HANDLE_VALUE) {
		_tprintf(TEXT("Create output file failed.\n"));
		goto failed;
	}
	
	while (sdc_http_check_data_len(request, &data_len) && data_len > 0) {
		result = sdc_http_recv(request, buffer, 4096, &bytes_read);
		if (!result) {
			_tprintf(TEXT("Read data failed, quit...\n"));
			goto failed;
		}

		// just write to file
		if (!WriteFile(hFile, buffer, bytes_read, &bytes_written, NULL)) {
			_tprintf(TEXT("Write buffer to file failed...\n"));
			goto failed;
		}
	}
	// close file
	CloseHandle(hFile);
	_tprintf(TEXT("Download finished...\n"));

	_getch();
	sdc_http_close(&session, &connect, &request);

	return 0;
failed:
	_getch();
	sdc_http_close(&session, &connect, &request);
	if (hFile != NULL && hFile != INVALID_HANDLE_VALUE) {
		CloseHandle(hFile);
	}
	return 1;
}

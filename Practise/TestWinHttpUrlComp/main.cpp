/*
 * Test WinHttp URL_COMPONENTS stuffs
 */

#include <windows.h>
#include <winhttp.h>
#include <tchar.h>
#include <conio.h>
#include <strsafe.h>

int _tmain(int argc, TCHAR *argv[], TCHAR *env[])
{
	BOOL result = FALSE;
	TCHAR test_url[] = TEXT("http://www.g.cn/getjobs?username=kartwall&password=111111");
	size_t test_url_len = 0;
	URL_COMPONENTS url_comp;

	// test WinHttpCrackUrl stuffs
    SecureZeroMemory(&url_comp, sizeof(url_comp));
    url_comp.dwStructSize = sizeof(url_comp);

    // Set required component lengths to non-zero 
    // so that they are cracked.
    url_comp.dwSchemeLength    = -1;
    url_comp.dwHostNameLength  = -1;
    url_comp.dwUrlPathLength   = -1;
    url_comp.dwExtraInfoLength = -1;

	StringCchLength(test_url, _countof(test_url), &test_url_len);
	result = WinHttpCrackUrl(test_url, test_url_len, 0, &url_comp);
	if (!result) {
		_tprintf(TEXT("Calling WinHttpCrackUrl failed, quit...\n"));
		goto failed;
	}
	// print url_comps stuffs
	_tprintf(TEXT("url_comp extra info: %s\nurl_comp hostname: %s\nurl_comp scheme: %s\nurl_comp path: %s\n"), 
		url_comp.lpszExtraInfo, url_comp.lpszHostName, url_comp.lpszScheme, url_comp.lpszUrlPath);

	_getch();
	return 0;
failed:
	_getch();
	return 1;
}

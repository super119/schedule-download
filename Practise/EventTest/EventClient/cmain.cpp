#include <windows.h>
#include <tchar.h>
#include <conio.h>

int _tmain(int argc, TCHAR *argv[], TCHAR *env[])
{
	HANDLE tevent;

	// Open the event
	tevent = OpenEvent(EVENT_MODIFY_STATE, FALSE, TEXT("EricTestEvent"));
	if (tevent == NULL) {
		_tprintf(TEXT("Cannot open event. Does EventServer is running?\n"));
		return 1;
	}

	// OK, notify the server
	_tprintf(TEXT("Get event. Begin notify EventServer...\n"));
	SetEvent(tevent);

	CloseHandle(tevent);
	return 0;
}
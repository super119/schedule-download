/*
 * Server must be started before client
 */


#include <windows.h>
#include <tchar.h>
#include <conio.h>

int _tmain(int argc, TCHAR *argv[], TCHAR *env[])
{
	HANDLE tevent;

	// Create an event
	tevent = CreateEvent(NULL, FALSE, FALSE, TEXT("EricTestEvent"));
	if (tevent == NULL) {
		_tprintf(TEXT("Create event failed, quit...\n"));
		return 1;
	}

	_tprintf(TEXT("Create event success, wait until client notify me...\n"));
	// Wait until client notify me
	WaitForSingleObject(tevent, INFINITE);

	_tprintf(TEXT("Got notification from client, Thanks, quit...\n"));

	CloseHandle(tevent);

	_getch();
	return 0;
}
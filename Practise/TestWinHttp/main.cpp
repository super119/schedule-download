#include <windows.h>
#include <tchar.h>
#include <winhttp.h>
#include <conio.h>
#include <locale.h>

BOOL convert_to_utf16(PSTR input, PTSTR output)
{
	return TRUE;
}

int _tmain(int argc, TCHAR *argv[], TCHAR *env[])
{
	DWORD dwDownloaded = 0;
	PSTR cl_buf = NULL;
	DWORD cl_buf_len;
	LPVOID content = NULL;
	BOOL bResults = FALSE;
	HINTERNET hSession = NULL, hConnect = NULL, hRequest = NULL;

	setlocale(LC_ALL, "chs");

	// Use WinHttpOpen to obtain a session handle.
	hSession = WinHttpOpen( L"WinHTTP Example/1.0",  
		WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
		WINHTTP_NO_PROXY_NAME, 
		WINHTTP_NO_PROXY_BYPASS, 0 );

	// Specify an HTTP server.
	if( hSession )
		hConnect = WinHttpConnect( hSession, L"www.microsoft.com",
		INTERNET_DEFAULT_HTTP_PORT, 0 );

	// Create an HTTP request handle.
	if( hConnect )
		hRequest = WinHttpOpenRequest( hConnect, L"GET", NULL,
		NULL, WINHTTP_NO_REFERER, 
		WINHTTP_DEFAULT_ACCEPT_TYPES, 
		NULL );

	// Send a request.
	if( hRequest )
		bResults = WinHttpSendRequest( hRequest,
		WINHTTP_NO_ADDITIONAL_HEADERS, 0,
		WINHTTP_NO_REQUEST_DATA, 0, 
		0, 0 );

	// End the request.
	if( bResults )
		bResults = WinHttpReceiveResponse( hRequest, NULL );

	// Keep checking for data until there is nothing left.
	if( bResults )
	{
		// get content length
		WinHttpQueryHeaders(hRequest, WINHTTP_QUERY_CONTENT_LENGTH, WINHTTP_HEADER_NAME_BY_INDEX, 
			WINHTTP_NO_OUTPUT_BUFFER, &cl_buf_len, WINHTTP_NO_HEADER_INDEX);
		if (GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
			_tprintf(L"Get content-length header buffer's need bytes: %d\n", cl_buf_len);
			cl_buf = (PSTR)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, cl_buf_len + 1);
		}
		
		if (WinHttpQueryHeaders(hRequest, WINHTTP_QUERY_CONTENT_LENGTH, WINHTTP_HEADER_NAME_BY_INDEX, 
			cl_buf, &cl_buf_len, WINHTTP_NO_HEADER_INDEX)) {
			cl_buf[cl_buf_len] = '\0';

			_tprintf(L"Get content-length header is %s\n", cl_buf);

			// alloc read buffer
			/*content = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, content_length + 1);
			
			// read all stuffs
			if( !WinHttpReadData( hRequest, content, content_length, &dwDownloaded ) ||
				dwDownloaded != content_length)
				_tprintf( L"Error %u in WinHttpReadData. dwDownloades is %d\n", GetLastError(), dwDownloaded);
			else
				((PSTR)content)[dwDownloaded] = '\0'; */

		} else {
			_tprintf(L"Get content-length header failed. Error code is %d\n", GetLastError());
		}
	} else {
		_tprintf( L"Error %d has occurred.\n", GetLastError());
	}

	// convert byte to Wide char

	

	HeapFree(GetProcessHeap(), NULL, content);
	// Close any open handles.
	if( hRequest ) WinHttpCloseHandle( hRequest );
	if( hConnect ) WinHttpCloseHandle( hConnect );
	if( hSession ) WinHttpCloseHandle( hSession );

	_getch();
	return 0;
}
﻿<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ page import="com.zhang.nicolas.common.Constants" %>
<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE>在哪都能自由下载,兼容迅雷、Rayfile网盘 - 自由自载</TITLE>
<META NAME="Keywords" CONTENT="软件,游戏,音乐,歌曲,影视,动漫,手机,书籍,下载,远程下载,迅雷,Rayfile,网盘">
<META NAME="Description" CONTENT="自由自载是一个方便您远程下载的网站。只需将需要下载的内容（支持迅雷和Rayfile），如bt种子文件，Rayfile网址上传到网站上，安装了自由自载客户端的电脑便可以自动开始下载，在网站上可以实时查看下载的情况。让您无论在什么地方和时间，都可以自由下载。您还可以共享下载的内容，让其他人也能分享您的精彩下载！">
<bean:define id="prefer_color" toScope="page">
	<bean:write name="<%=Constants.SESSION_USER_OBJ_KEY%>" property="usercolor" scope="session"/>
</bean:define>
<link rel="stylesheet" type="text/css" href="../css/sdweb-<%=prefer_color%>.css"/>
<html:base/>
</head>

<body>
<table width="100%" align=center cellpadding=0 cellspacing=5 border=0>
    <tr>
        <td rowspan="2" align="left">
            <html:link action="HomepageAction">
            	<img src="<bean:message key='jsp.index.logopath' arg0='<%=prefer_color%>'/>"
            		 alt="<bean:message key='jsp.index.logoalt'/>" border="0" />
            </html:link>
        </td>
    </tr>
    <tr>
        <td align="right">
            <logic:notEmpty name="<%=Constants.SESSION_USER_OBJ_KEY%>" property="username" scope="session">
                <bean:write name="<%=Constants.SESSION_USER_OBJ_KEY%>" property="username" scope="session" />
                <bean:message key="jsp.common.ecomma" />
                <html:link action="UserCPAction">
                    <bean:message key="jsp.index.usercp"/>
                </html:link>&nbsp;
                <bean:message key="jsp.common.vline"/>&nbsp;
                <html:link action="LogoutAction">
                    <bean:message key="jsp.index.logout"/>
                </html:link>&nbsp;
            </logic:notEmpty>
            
            <logic:empty name="<%=Constants.SESSION_USER_OBJ_KEY%>" property="username" scope="session">
                <html:link action="GLoginAction">
                    <bean:message key="jsp.index.login"/>
                </html:link>&nbsp;
                <bean:message key="jsp.common.vline"/>&nbsp;
                <html:link action="GRegisterAction">
                    <bean:message key="jsp.index.register"/>
                </html:link>&nbsp;
            </logic:empty>

            <bean:message key="jsp.common.vline"/>&nbsp;
            <html:link action="HomepageAction">
                <bean:message key="jsp.index.homepage"/>
            </html:link>&nbsp;
            <bean:message key="jsp.common.vline"/>&nbsp;
            <html:link action="GCommentAction">
                <bean:message key="jsp.index.comment"/>
            </html:link>&nbsp;
            <bean:message key="jsp.common.vline"/>&nbsp;
            <html:link action="GHelpAction">
                <bean:message key="jsp.index.help"/>
            </html:link>&nbsp;
            <bean:message key="jsp.common.vline"/>&nbsp;
            <html:link action="GWebmapAction">
                <bean:message key="jsp.index.webmap"/>
            </html:link>
        </td>

        <td align="right">
        	<html:errors/>&nbsp;&nbsp;
            <html:form action="FullSearchAction">
                <html:text property="searchKey" size="20"/>
                <input type="image" src="<bean:message key='jsp.index.searchiconpath' arg0='<%=prefer_color%>'/>"
            		 alt="<bean:message key='jsp.index.searchiconalt'/>" border="0" />
            </html:form>
        </td>
    </tr>
</table>

<table width="100%" align=center cellpadding=0 cellspacing=0 border=0>
    <tr><td height="10"></td></tr>
</table>

<table width="100%" align=center cellpadding=0 cellspacing=0 border=0 >
    <tr><td height="1" class="separateline"></td></tr>
</table>

<table width="100%" align=center cellpadding=0 cellspacing=0 border=0>
    <tr><td height="10"></td></tr>
</table>

<table width="100%" align="center" cellpadding="0" cellspacing="10" border="0">
    <tr>
        <!-- Latest downloads info -->
        <td width="100%">
            <table width="100%" align="left" cellpadding="0" cellspacing="0" class="innertable">

            </table>
        </td>
        
        <!-- User downloads info -->
        <td width="350">

        </td>
    </tr>
</table>

<table width="100%" align=center cellpadding=0 cellspacing=0 border=0>
    <tr><td height="10"></td></tr>
</table>

<!-- bottom part -->
<table width="100%" align=center cellpadding=0 cellspacing=0 border=0>
    <tr>
        <td width="100%" align="center">
            <bean:message key="jsp.index.copyright"/>
        </td>
    </tr>
</table>

</body>
</html>

/*
 * HomepageBean.java
 *
 */

package com.zhang.nicolas.logic;

import java.sql.*;
import com.zhang.nicolas.common.*;
import org.apache.commons.logging.*;
import java.util.ArrayList;
import java.lang.reflect.*;

/**
 *
 */
public class HomepageBean extends BaseBean {

    /** Creates a new instance of HomepageBean */
    public HomepageBean() {
    }

    /*
     * Get a log instance
     */
    private Log log = LogFactory.getLog(HomepageBean.class);

    /**
     * Business function
     */
    public String getHomePageInformation(String username) {
        log.info("<getHomePageInformation>: Entering getHomePageInformation function......");
        WebToolKit kit = WebToolKit.getInstance();
        /* try {
        	// check out latest downloads info
        	
        	
        	if (username == null) {
        		// no user login in, ignore checking out user downloads info
        	}
            

        } catch(SQLException e1) {
            log.error("<getHomePageInformation>: SQLException: ", e1);
            return "error.system.sqlexception";
        } catch(ClassNotFoundException e2) {
            log.error("<getHomePageInformation>: ClassNotFoundException: ", e2);
            return "error.system.reflectexception";
        } catch(InstantiationException e3) {
            log.error("<getHomePageInformation>: InstantiationException: ", e3);
            return "error.system.reflectexception";
        } catch(IllegalAccessException e4) {
            log.error("<getHomePageInformation>: IllegalAccessException: ", e4);
            return "error.system.reflectexception";
        } catch(NoSuchMethodException e5) {
            log.error("<getHomePageInformation>: NoSuchMethodException: ", e5);
            return "error.system.reflectexception";
        } catch(InvocationTargetException e6) {
            log.error("<getHomePageInformation>: InvocationTargetException: ", e6);
            return "error.system.reflectexception";
        } catch (Exception e7) {
            log.error("<getHomePageInformation>: Exception: ", e7);
            return "error.system.unknown";
        } finally {
            log.info("<getHomePageInformation>: Entering finally block, sweeping the house...");
            try {
                releaseDBResources();
            } catch (Exception e8) {
                log.error("<getHomePageInformation>: Sweeping house at finally block exception: ", e8);
            }
        } */

        log.info("<getHomePageInformation>: Leaving getHomePageInformation function......");
        return null;
    }
}
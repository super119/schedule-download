/*
 * BaseBean.java
 *
 */

package com.zhang.nicolas.logic;

import com.zhang.nicolas.common.*;
import javax.sql.*;
import java.sql.*;
import java.net.*;
import java.util.*;
import java.lang.reflect.*;
import org.apache.commons.logging.*;

/**
 * @author Eric Zhang
 */
public class BaseBean {

    /** Creates a new instance of BaseBean */
    public BaseBean() {
    }

    /*
     * Get a log instance
     * Many beans will extend from basebean
     * so we should define this.getClass here
     */
    private Log log = LogFactory.getLog(BaseBean.class);

    /*
     * Define config info
     */
    protected SDConfig sdconfig = null;

    public SDConfig getSdconfig() {
        return sdconfig;
    }
    public void setSdconfig(Object value) {
        sdconfig = null;
        sdconfig = (SDConfig)value;
    }

    /*
     * Cache JNDI lookup result - DataSource
     */
    protected DataSource datasource = null;
    public DataSource getDatasource() {
        return datasource;
    }
    public void setDatasource(Object value) {
        datasource = (DataSource)value;
    }

    // ============================================================= DataBase Operation
    /**
     * The connection of the database
     */
    protected Connection conn = null;

    public Connection getConn() throws SQLException {
        if (conn == null) {
            conn = datasource.getConnection();
        }
        return conn;
    }

    /**
     * The container of db statements
     */
    protected HashMap statements = new HashMap();
    /**
     * Get statements from connection
     * the new statement will be registered in statements instance
     * so that we can release all statements in this basebean
     */
    public Statement getStmt() throws SQLException {
        Statement tmp_stmt = conn.createStatement();
        int statements_size = statements.size();
        String register_key = "statement" + Integer.toString(statements_size);
        statements.put(register_key, tmp_stmt);

        return tmp_stmt;
    }

    /**
     * The container of db resultset
     */
    protected HashMap resultsets = new HashMap();
    /**
     * Define a temp resultset to be used by classes which extend from basebean
     */
    protected ResultSet tmp_rst = null;

    /**
     * do Query action
     */
    public ResultSet startQuery(String sql) throws SQLException {
        // Get DataBase connection first
        getConn();

        // Create Statement
        Statement tmp_stmt = getStmt();

        // Set correct character set
        tmp_stmt.execute("SET NAMES UTF8");

        // Get resultset
        ResultSet tmp_rst = tmp_stmt.executeQuery(sql);

        int resultsets_size = resultsets.size();
        String register_key = "resultset" + Integer.toString(resultsets_size);
        resultsets.put(register_key, tmp_rst);

        return tmp_rst;
    }

    /**
     * do Update action
     */
    public void startUpdate(String sql) throws SQLException {
        // Get DataBase connection first
        getConn();

        // Create Statement
        Statement tmp_stmt = getStmt();

        // Set correct character set
        tmp_stmt.execute("SET NAMES UTF8");

        tmp_stmt.executeUpdate(sql);
    }

    /**
     * Release all database resources
     */
    public void releaseDBResources() throws SQLException {
        // Release resultsets
        Iterator it = resultsets.keySet().iterator();
        while (it.hasNext())
        {
            String key = (String)it.next();
            ResultSet temprs = (ResultSet)resultsets.get(key);
            if (temprs != null) {
                temprs.close();
                temprs = null;
            }
        }
        resultsets.clear();

        // Release statements
        it = statements.keySet().iterator();
        while (it.hasNext())
        {
            String key = (String)it.next();
            Statement tempst = (Statement)statements.get(key);
            if (tempst != null) {
                tempst.close();
                tempst = null;
            }
        }
        statements.clear();

        // Release Connection
        if (conn != null) {
            if (!conn.isClosed()) conn.close();
            conn = null;
        }

        // release tmp_rst
        tmp_rst = null;
    }

    /**
     * Fill the Form/Entity Class Instances using Java Reflection
     */
    public ArrayList dbReflectFillBunkData(String sql, String className)
            throws SQLException, ClassNotFoundException, InstantiationException,
                   IllegalAccessException, NoSuchMethodException, InvocationTargetException
    {
        ArrayList container = new ArrayList();
        // execute the sql now
        tmp_rst = startQuery(sql);

        ResultSetMetaData rsmd = tmp_rst.getMetaData();
        int columnCount = rsmd.getColumnCount();

        // Data Container
        while (tmp_rst.next()) {
            Object c1 = Class.forName(className).newInstance();
            String recordValue = null;
            for (int i=1; i<=columnCount; i++) {
                if (tmp_rst.getString(rsmd.getColumnName(i)) != null) {
                    recordValue = tmp_rst.getString(rsmd.getColumnName(i));
                } else {
                    recordValue = "";
                }

                Method m = c1.getClass().getMethod(getSetMethodName(rsmd.getColumnName(i)),
                                                        new Class[]{recordValue.getClass()});
                m.invoke(c1, new Object[]{recordValue});
            }
            // Add this model instance into container
            container.add(c1);
        }

        return container;
    }

    /**
     * Fill the Form/Entity Class Instances using Java Reflection
     */
    public Object dbReflectFillData(String sql, String className)
            throws SQLException, ClassNotFoundException, InstantiationException,
                   IllegalAccessException, NoSuchMethodException, InvocationTargetException
    {
        // execute the sql now
        tmp_rst = startQuery(sql);

        WebToolKit kit = WebToolKit.getInstance();
        if (!kit.resultsethasrecords(tmp_rst)) {
            // no records matched, return null
            // to avoid throwing the SQLException
            // resultset will move to the first record after using resultsethasrecords function
            return null;
        }

        ResultSetMetaData rsmd = tmp_rst.getMetaData();
        int columnCount = rsmd.getColumnCount();

        // Data Container
        Object c1 = Class.forName(className).newInstance();
        String recordValue = null;
        for (int i=1; i<=columnCount; i++) {
            if (tmp_rst.getString(rsmd.getColumnName(i)) != null) {
                recordValue = tmp_rst.getString(rsmd.getColumnName(i));
            } else {
                recordValue = "";
            }

            Method m = c1.getClass().getMethod(getSetMethodName(rsmd.getColumnName(i)),
                                               new Class[]{recordValue.getClass()});
            m.invoke(c1, new Object[]{recordValue});
        }

        return c1;
    }

    //==================================================  Common Toolkit Functions

    /**
     * Generate the model class's function name
     **/
    private String getSetMethodName(String input) {
        if (input == null || input.trim().length() < 1)
            return null;

        String result = "set" + (input.substring(0, 1).toUpperCase()) + input.substring(1);
        // log.info("<getSetMethodName>: Input is: " + input + " and the result is: " + result);
        return result;
    }

}
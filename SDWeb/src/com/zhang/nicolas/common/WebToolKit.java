/*
 * WebToolKit.java
 *
 */

package com.zhang.nicolas.common;

import java.sql.*;
import java.util.Calendar;
import java.security.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class WebToolKit {
    private static WebToolKit  _instance = null;

    private WebToolKit() {
    }

    public static WebToolKit  getInstance() {
        if (_instance == null) {
            _instance = new WebToolKit();
        }
        return _instance;
    }


    /**
     * whether input is an integer?
     */
    public boolean isint(String input) {
        if (input == null || input.trim().length() <= 0)
            return false;

        boolean bresult = true;
        try {
            Integer.parseInt(input);
        } catch (Exception e) {
            bresult = false;
        }
        return bresult;
    }

    /**
     * whether input is an positive integer?
     */
    public boolean isPositiveInt(String input) {
        if (input == null || input.trim().length() <= 0)
            return false;

        boolean bresult = true;
        int resultnumber = -1;
        try {
            resultnumber = Integer.parseInt(input);
        } catch (Exception e) {
            bresult = false;
        }

        if (resultnumber <= 0)
            bresult = false;

        return bresult;
    }

    /**
     * whether input is an positive integer or zero
     */
    public boolean isPositiveIntOrZero(String input) {
        if (input == null || input.trim().length() <= 0)
            return false;

        boolean bresult = true;
        int resultnumber = -1;
        try {
            resultnumber = Integer.parseInt(input);
        } catch (Exception e) {
            bresult = false;
        }

        if (resultnumber < 0)
            bresult = false;

        return bresult;
    }

    /**
     * whether input is an positive float?
     */
    public boolean isPositiveFloat(String input) {
        if (input == null || input.trim().length() <= 0)
            return false;

        boolean bresult = true;
        float resultnumber = -1;
        try {
            resultnumber = Float.parseFloat(input);
        } catch (Exception e) {
            bresult = false;
        }

        if (resultnumber <= 0)
            bresult = false;

        return bresult;
    }

    /**
     * whether input is an positive float or zero
     */
    public boolean isPositiveFloatOrZero(String input) {
        if (input == null || input.trim().length() <= 0)
            return false;

        boolean bresult = true;
        float resultnumber = -1;
        try {
            resultnumber = Float.parseFloat(input);
        } catch (Exception e) {
            bresult = false;
        }

        if (resultnumber < 0)
            bresult = false;

        return bresult;
    }

    /**
     * whether input is a float?
     */
    public boolean isfloat(String input) {
        boolean bresult = true;
        try {
            Float.parseFloat(input);
        } catch (Exception e) {
            bresult = false;
        }
        return bresult;
    }

    /**
     * whether input is a double?
     */
    public boolean isDouble(String input) {
        boolean bresult = true;
        try {
            Double.parseDouble(input);
        } catch (Exception e) {
            bresult = false;
        }
        return bresult;
    }

    /**
     * whether the resultset has records in it? CAUTION: ResultSet will move to the first record after using this method
     * @param resultset
     * @return
     */
    public boolean resultsethasrecords(ResultSet resultset) {
        if (resultset == null) {
            return false;
        }

        try {
            if (resultset.next()) {
                //has records
                //resultset.beforeFirst();
                return true;
            }
        } catch (SQLException e) {
            return false;
        }

        return false;
    }

    /**
     * get result record count. CAUTION: ResultSet pointer will move to the before-first position
     * after calling this function
     * @param resultset
     * @return
     */
    public int getResultSetRecordCount(ResultSet resultset) {
        int recordCount = 0;
        if (resultset == null) {
            return -1;
        }

        try {
            while (resultset.next()) {
                recordCount++;
            }

            // recover resultset's point
            if (recordCount > 0)
                resultset.beforeFirst();
        } catch (SQLException e) {
            return -1;
        }

        return recordCount;
    }

    /**
     * Get current time, such as XXXX-XX-XX XX:XX:XX
     * @return
     */
    public String getcurrenttime() {
        String year = Integer.toString(Calendar.getInstance().get(Calendar.YEAR));
        String month = Integer.toString(Calendar.getInstance().get(Calendar.
                MONTH) + 1);
        if (month.length() < 2)
            month = "0" + month;
        String day = Integer.toString(Calendar.getInstance().get(Calendar.
                DAY_OF_MONTH));
        if (day.length() < 2)
            day = "0" + day;
        String hour = Integer.toString(Calendar.getInstance().get(Calendar.
                HOUR_OF_DAY));
        if (hour.length() < 2)
            hour = "0" + hour;
        String minute = Integer.toString(Calendar.getInstance().get(Calendar.
                MINUTE));
        if (minute.length() < 2)
            minute = "0" + minute;
        String second = Integer.toString(Calendar.getInstance().get(Calendar.
                SECOND));
        if (second.length() < 2)
            second = "0" + second;

        String currenttime = year + "-" + month + "-" + day + " " + hour + ":" +
                minute + ":" + second;

        return currenttime;
    }

    /**
     * Get Simplify time, such as XX-XX-XX XX:XX
     * @return
     */
    public String GetSimplifyTime() {
        String year = Integer.toString(Calendar.getInstance().get(Calendar.YEAR));
        year = year.substring(2);
        String month = Integer.toString(Calendar.getInstance().get(Calendar.
                MONTH) + 1);
        if (month.length() < 2)
            month = "0" + month;
        String day = Integer.toString(Calendar.getInstance().get(Calendar.
                DAY_OF_MONTH));
        if (day.length() < 2)
            day = "0" + day;
        String hour = Integer.toString(Calendar.getInstance().get(Calendar.
                HOUR_OF_DAY));
        if (hour.length() < 2)
            hour = "0" + hour;
        String minute = Integer.toString(Calendar.getInstance().get(Calendar.
                MINUTE));
        if (minute.length() < 2)
            minute = "0" + minute;

        String currenttime = year + "-" + month + "-" + day + " " + hour + ":" + minute;

        return currenttime;
    }

    /**
     * Get current date, such as XXXX-XX-XX
     * @return
     */
    public String GetCurrentDate() {
        String year = Integer.toString(Calendar.getInstance().get(Calendar.YEAR));
        String month = Integer.toString(Calendar.getInstance().get(Calendar.
                MONTH) + 1);
        if (month.length() < 2)
            month = "0" + month;
        String day = Integer.toString(Calendar.getInstance().get(Calendar.
                DAY_OF_MONTH));
        if (day.length() < 2)
            day = "0" + day;

        String currentDate = year + "-" + month + "-" + day;

        return currentDate;
    }

    public String ProcessNullString(String input) {
        if (input == null || input.trim().length() == 0)
            return " ";
        else
            return input;
    }

    /**
     * MD5 Crypt
     * @param csinput
     * @return
     */
    public String CryptPassword(String csinput) {
        byte[] b, b2;
        StringBuffer buf;
        String csreturn = null;

        try {
            b = csinput.getBytes("iso-8859-1");
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(b);
            b2 = md.digest();

            buf = new StringBuffer(b2.length * 2);
            for (int nLoopindex = 0; nLoopindex < b2.length; nLoopindex++) {
                if ( ( (int) b2[nLoopindex] & 0xff) < 0x10) {
                    buf.append("0");
                }
                buf.append(Long.toString( (int) b2[nLoopindex] & 0xff, 16));
            }
            csreturn = new String(buf);
        } catch (Exception e) {
            e.printStackTrace();
            csreturn = null;
        }

        return csreturn;
    }

    /**
     * Add Bytes - used by socket action
     */
    public byte[] addByteBuffers(byte[] first, byte[] second) {
        int nloopindex = 0;
        int first_length = first.length;
        byte[] return_bytes;
        if (second == null) {
            // just to copy the first byte[]
            return_bytes = new byte[first_length];

            for (nloopindex = 0; nloopindex < first_length; nloopindex++) {
                return_bytes[nloopindex] = first[nloopindex];
            }
        } else {
            int second_length = second.length;
            return_bytes = new byte[first_length + second_length];

            for (nloopindex = 0; nloopindex < first_length; nloopindex++) {
                return_bytes[nloopindex] = first[nloopindex];
            }
            for (nloopindex = 0; nloopindex < second_length; nloopindex++) {
                return_bytes[first_length + nloopindex] = second[nloopindex];
            }
        }

        return return_bytes;
    }

    /**
     * Judge whether the string is contained with letter, number and underline
     */
    public boolean isLNUFormat(String input) {
        boolean result = true;
        for (int nloop = 0; nloop < input.length(); nloop++) {
            int char_tmp = (int)input.charAt(nloop);
            // A-Z is 65-90   a-z is 97-122  _ is 95   0-9 is 48-57
            if (!((char_tmp >= 65 && char_tmp <= 90) ||
                    (char_tmp >= 97 && char_tmp <= 122) ||
                    (char_tmp >= 48 && char_tmp <= 57) ||
                    char_tmp == 95)) {
                result = false;
                break;
            }
        }
        return result;
    }

    /**
     * Judge whether the string is contained with letter and number
     */
    public boolean isLNFormat(String input) {
        boolean result = true;
        for (int nloop = 0; nloop < input.length(); nloop++) {
            int char_tmp = (int)input.charAt(nloop);
            // A-Z is 65-90   a-z is 97-122  _ is 95   0-9 is 48-57
            if (!((char_tmp >= 65 && char_tmp <= 90) ||
                    (char_tmp >= 97 && char_tmp <= 122) ||
                    (char_tmp >= 48 && char_tmp <= 57))) {
                result = false;
                break;
            }
        }
        return result;
    }

    /**
     * Judge whether the string starts with letter
     */
    public boolean isStartWithLetter(String input) {
        char tmp = input.charAt(0);
        if ((tmp >= 65 && tmp <= 90) || (tmp >= 97 && tmp <= 122))
            return true;
        else
            return false;
    }

    /**
     * Judge whether a string conforms to the date-time syntax, just like XXXX-XX-XX XX:XX
     * or XXXX-XX-XX XX:XX:XX
     */
    public boolean isDateTimeConform(String inputstring) {
        if (inputstring == null || inputstring.trim().length() <= 0)
            return false;

        // we check every part of the string to consider the result
        String localstring = inputstring.trim();
        String tmpstring = null;
        int tmpint = -1;

        // year?
        int localindex = localstring.indexOf("-");
        if (localindex == -1) {
            return false;
        } else {
            tmpstring = localstring.substring(0, localindex);
            tmpint = String2Int(tmpstring);
            if (tmpint < 2000 || tmpint > 9999) return false;
            localstring = localstring.substring(localindex + 1);
        }

        // month?
        localindex = localstring.indexOf("-");
        if (localindex == -1) {
            return false;
        } else {
            tmpstring = localstring.substring(0, localindex);
            tmpint = String2Int(tmpstring);
            if (tmpint < 1 || tmpint > 12 || tmpstring.length() < 2) return false;
            localstring = localstring.substring(localindex + 1);
        }

        // day?
        localindex = localstring.indexOf(" ");
        if (localindex == -1) {
            return false;
        } else {
            tmpstring = localstring.substring(0, localindex);
            tmpint = String2Int(tmpstring);
            if (tmpint < 1 || tmpint > 31 || tmpstring.length() < 2) return false;
            localstring = localstring.substring(localindex + 1).trim();
        }

        // hour?
        localindex = localstring.indexOf(":");
        if (localindex == -1) {
            return false;
        } else {
            tmpstring = localstring.substring(0, localindex);
            tmpint = String2Int(tmpstring);
            if (tmpint < 0 || tmpint > 23 || tmpstring.length() < 2) return false;
            localstring = localstring.substring(localindex + 1);
        }

        // minute?
        localindex = localstring.indexOf(":");
        if (localindex == -1) {
            // XXXX-XX-XX XX:XX syntax?
            tmpint = String2Int(localstring);
            if (tmpint < 0 || tmpint > 59 || localstring.length() < 2) return false;
        } else {
            // XXXX-XX-XX XX:XX:XX syntax?
            tmpstring = localstring.substring(0, localindex);
            tmpint = String2Int(tmpstring);
            if (tmpint < 0 || tmpint > 59 || tmpstring.length() < 2) return false;
        }

        // success
        return true;
    }

    /**
     * Judge whether a string conforms to the simplified date syntax, just like XXXX-XX-XX
     */
    public boolean isDateConform(String inputstring) {
        if (inputstring == null || inputstring.trim().length() <= 0)
            return false;

        // we check every part of the string to consider the result
        String localstring = inputstring.trim();
        String tmpstring = null;
        int tmpint = -1;

        // year?
        int localindex = localstring.indexOf("-");
        if (localindex == -1) {
            return false;
        } else {
            tmpstring = localstring.substring(0, localindex);
            tmpint = String2Int(tmpstring);
            if (tmpint < 2000 || tmpint > 9999) return false;
            localstring = localstring.substring(localindex + 1);
        }

        // month?
        localindex = localstring.indexOf("-");
        if (localindex == -1) {
            return false;
        } else {
            tmpstring = localstring.substring(0, localindex);
            tmpint = String2Int(tmpstring);
            if (tmpint < 1 || tmpint > 12 || tmpstring.length() < 2) return false;
            localstring = localstring.substring(localindex + 1);
        }

        // day?  the string left must be a day
        tmpint = String2Int(localstring);
        if (tmpint < 1 || tmpint > 31 || tmpstring.length() < 2) return false;

        // success
        return true;
    }

    /**
     * String to integer
     */
    public int String2Int(String input) {
        int result = -1;
        try {
            result = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            result = -1;
        }
        return result;
    }

    /**
     * String to double
     */
    public double String2Double(String input) {
        double result = -1;
        try {
            result = Double.parseDouble(input);
        } catch (NumberFormatException e) {
            result = -1;
        }
        return result;
    }

   /*
    * Convert byte[4] to int
    */
    public int Byte2Int(byte[] buf, boolean asc) {
        if (buf == null) {
            // illegal input
            return -1;
        }
        if (buf.length > 4) {
            // illegal input
            return -1;
        }
        int r = 0;
        if (asc) {
            for (int i = buf.length - 1; i >= 0; i--) {
                r <<= 8;
                r |= (buf[i] & 0xff);
            }
        } else {
            for (int i = 0; i < buf.length; i++) {
                r <<= 8;
                r |= (buf[i] & 0xff);
            }
        }
        return r;
    }

    /**
     * Convert int to byte[4]
     */
    public byte[] Int2Byte(int s, boolean asc) {
        byte[] buf = new byte[4];
        if (asc) {
            for (int i = buf.length - 1; i >= 0; i--) {
                buf[i] = (byte) (s & 0x000000ff);
                s >>= 8;
            }
        } else {
            for (int i = 0; i < buf.length; i++) {
                buf[i] = (byte) (s & 0x000000ff);
                s >>= 8;
            }
        }
        return buf;
    }

    /*
     * Check whether the specified @entry exists in cookie
     */
    public String CheckCookies(HttpServletRequest request, String entry) {
    	if (request == null || entry == null)
    		return null;
    	
        Cookie[] cookie = request.getCookies();
        if (cookie != null && cookie.length > 0) {
            for (int i=0; i < cookie.length; i++) {
        		if (cookie[i].getName().equals(entry) && cookie[i].getValue() != null) {
                	return cookie[i].getValue();
            	}
        	}
        }
    
        return null;
    }


    public static void main(String argc[]) {
        //WebToolKit kit = WebToolKit.getInstance();
        //System.out.println(kit.isLNUFormat("Hel3_helo#"));
        //System.out.println(kit.isStartWithLetter("$Hel3_helo#"));

        // WebToolKit kit = WebToolKit.getInstance();
        // byte[] QUERY_RUNNING_JOB = {0x00, 0x0d, 0x00, 0x00};
        // System.out.println(kit.Byte2Int(QUERY_RUNNING_JOB, false));

        /* String input = "1::1";
        String[] token_array = input.split(":");
        System.out.println(token_array.length);
        for (int i=0; i<token_array.length; i++) {
            System.out.println(token_array[i]);
        } */

        // WebToolKit kit = WebToolKit.getInstance();
        // System.out.println(kit.String2Int("00"));

        // WebToolKit kit = WebToolKit.getInstance();
        // System.out.println(kit.isSGETimeFormat("1::2323"));

        // WebToolKit kit = WebToolKit.getInstance();
        // System.out.println(kit.isSGEMemoryFormat("1s"));

        String test_string = "FS";
        char first_char = test_string.charAt(0);
        if (first_char == 'O' || first_char == 'o') {
            System.out.println("Success");
        } else {
            System.out.println("Failed");
        }
    }
}
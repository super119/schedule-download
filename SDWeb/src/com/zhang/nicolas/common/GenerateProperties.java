/*
 * GenerateProperties.java
 */

package com.zhang.nicolas.common;

import org.apache.oro.text.regex.*;
import java.io.*;
import java.util.*;

/**
 *
 * @author Eric
 */
public class GenerateProperties {
    /**
     * Creates a new instance of GenerateProperties
     */
    public GenerateProperties() {
    }
    
    //============================================================== Definition BEGIN ===================================================================================================//
    
    // jsp file reside directory path
    private String jsp_directory = "D:\\ScheduleDownload\\SDWeb\\web\\jsp";
    
    // java file reside directory path
    private String java_directory = "D:\\ScheduleDownload\\SDWeb\\src\\com\\zhang\\nicolas";
    
    // property file
    private String property_file = "D:\\ScheduleDownload\\SDWeb\\src\\com\\zhang\\nicolas\\resource\\ApplicationResource.properties";
    
    // recursive jsp directory?
    private boolean recursive_jsp_directory = true;
    
    // recursive java directory
    private boolean recursive_java_directory = true;
    
    // jsp filter Regular String
    String jsp_restring = "['\"](jsp|error)\\.[^'\"]+['\"]";
    
    // java filter Regular String
    String java_restring = "['\"](action|bean|error|form)\\.[^'\"]+['\"]";
    
    // All filenames container
    private ArrayList fileContainer = new ArrayList();
    
    // properties container
    private Properties props = new Properties();
    
    //============================================================== Definition END ===================================================================================================//
    
    /**
     * List files function, using recursion
     */
    private void searchFiles(String dir, String jsp_or_java) {
                
        File root = new File(dir);
        File[] file_array = root.listFiles();
        
        for (int i=0; i<file_array.length; i++) {
            if (file_array[i].isDirectory()) {
                if (jsp_or_java.equalsIgnoreCase("jsp")) {
                    if (recursive_jsp_directory) {
                        searchFiles(file_array[i].getAbsolutePath(), jsp_or_java);
                    }
                } else if (jsp_or_java.equalsIgnoreCase("java")) {
                    if (recursive_java_directory) {
                        searchFiles(file_array[i].getAbsolutePath(), jsp_or_java);
                    }
                }
            } else {
                fileContainer.add(file_array[i].getAbsolutePath());
            }
        }
    }
    
    /**
     * Load properties
     */
    private void LoadProperties() {
        InputStream is = null;
        
        try {
            // Looking for properties file
            is = new FileInputStream(property_file);
        
            if (is != null) {
                props.load(is);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (is != null)
                    is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Process JSP files
     */
    public void DistillJSP() {
        fileContainer.clear();
        
        // Get all filenames under JSP directory
        searchFiles(jsp_directory, "jsp");
        
        LoadProperties();
        
        try {
            // Prepare the Writers
            FileWriter props_writer = new FileWriter(property_file, true);
            BufferedWriter props_bufwriter = new BufferedWriter(props_writer);
        
            // Iterate the files
            int file_number = fileContainer.size();
            // The result string container
            HashMap result_container = new HashMap();
            
            for (int i=0; i<file_number; i++) {
                // Ignore files don't end with `jsp'
                String filename = (String)fileContainer.get(i);
                if (filename == null || filename.trim().length() < 1 ||
                    !filename.substring(filename.length() - 3, filename.length()).equalsIgnoreCase("jsp")) {
                    continue;
                }
                
                // Open the JSP File and read out the resources
                FileReader jsp_file_reader = new FileReader(filename);
                BufferedReader buffer_reader = new BufferedReader(jsp_file_reader);
                String input_line = null;
                
                PatternCompiler orocom = new Perl5Compiler();
                Pattern pattern = orocom.compile(jsp_restring);
                PatternMatcher matcher = new Perl5Matcher();
                
                while((input_line = buffer_reader.readLine()) != null) {
                    // System.out.println("Got one line from jsp file: " + input_line);
                    
                    // Begin filter
                    // Use PatternMatcherInput class, we can find the next string in one line string
                    // Otherwise ORO will find the first match string in one line then ignore the rest string of this line
                    PatternMatcherInput reinput = new PatternMatcherInput(input_line);
                    while (matcher.contains(reinput, pattern)) {
                        MatchResult result = matcher.getMatch();
                        String result_string = result.group(0);
                        
                        // Get rid of the quotations
                        result_string = result_string.substring(1, result_string.length() - 1);
                        // if we got the duplicated result_string, HashMap will overcome this issue
                        result_container.put(result_string, "dummy object");
                    }
                }
                
                buffer_reader.close();
                jsp_file_reader.close();
            }
            
            // Iterate the HashMap and write the result
            Iterator it = result_container.keySet().iterator();
            while (it.hasNext()) {
                String key = (String)it.next();
                
                // check whether this key has been added?
                if (props.getProperty(key) == null) {
                    System.out.println("Found one resource: " + key);
                    props_bufwriter.newLine();
                    props_bufwriter.write(key + "=dummy");
                    props_bufwriter.newLine();
                }
            }
    
            props_bufwriter.close();
            props_writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Process JAVA Files
     */
    public void DistillJava() {
        fileContainer.clear();
        
        // Get all filenames under Java directory
        searchFiles(java_directory, "java");
        
        // Load root, portal, admin current properties
        LoadProperties();
        
        try {
            // Prepare the Writers
            FileWriter props_writer = new FileWriter(property_file, true);
            BufferedWriter props_bufwriter = new BufferedWriter(props_writer);
        
            // Iterate the files
            int file_number = fileContainer.size();
            // The result string container
            HashMap result_container = new HashMap();
            
            for (int i=0; i<file_number; i++) {
                // Ignore files don't end with `java'
                String filename = (String)fileContainer.get(i);
                if (filename == null || filename.trim().length() < 1 ||
                    !filename.substring(filename.length() - 4, filename.length()).equalsIgnoreCase("java")) {
                    continue;
                }
                
                // Open the Java File and read out the resources
                FileReader java_file_reader = new FileReader(filename);
                BufferedReader buffer_reader = new BufferedReader(java_file_reader);
                String input_line = null;
                
                PatternCompiler orocom = new Perl5Compiler();
                Pattern pattern = orocom.compile(java_restring);
                PatternMatcher matcher = new Perl5Matcher();
                
                while((input_line = buffer_reader.readLine()) != null) {
                    // System.out.println("Got one line from java file: " + input_line);
                    
                    // Begin filter
                    // Use PatternMatcherInput class, we can find the next string in one line string
                    // Otherwise ORO will find the first match string in one line then ignore the rest string of this line
                    PatternMatcherInput reinput = new PatternMatcherInput(input_line);
                    while (matcher.contains(reinput, pattern)) {
                        MatchResult result = matcher.getMatch();
                        String result_string = result.group(0);
                        
                        // Get rid of the quotations
                        result_string = result_string.substring(1, result_string.length() - 1);
                        // if we got the duplicated result_string, HashMap will overcome this issue
                        result_container.put(result_string, "dummy object");
                    }
                }
                
                buffer_reader.close();
                java_file_reader.close();
            }
                
            // Iterate the HashMap and write the result
            Iterator it = result_container.keySet().iterator();
            while (it.hasNext()) {
                String key = (String)it.next();
                
                // check whether this key has been added?
                if (props.getProperty(key) == null) {
                    System.out.println("Found one resource: " + key);
                    props_bufwriter.newLine();
                    props_bufwriter.write(key + "=dummy");
                    props_bufwriter.newLine();
                }
            }
                            
            props_bufwriter.close();
            props_writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        GenerateProperties handle = new GenerateProperties();
        handle.DistillJSP();
        handle.DistillJava();
    }
    
}

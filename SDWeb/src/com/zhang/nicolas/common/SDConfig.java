/*
 * SDConfig.java
 *
 */

package com.zhang.nicolas.common;

/**
 *
 */
public final class SDConfig {

    /** Creates a new instance of SDConfig */
    public SDConfig() {
    }

    // Define properties
    private String dataSourceName = null;
    private String webContextName = null;

    /**
     * @return the dataSourceName
     */
    public String getDataSourceName() {
        return dataSourceName;
    }

    /**
     * @param dataSourceName the dataSourceName to set
     */
    public void setDataSourceName(String dataSourceName) {
        this.dataSourceName = dataSourceName;
    }

    /**
     * @return the webContextName
     */
    public String getWebContextName() {
        return webContextName;
    }

    /**
     * @param webContextName the webContextName to set
     */
    public void setWebContextName(String webContextName) {
        this.webContextName = webContextName;
    }

}

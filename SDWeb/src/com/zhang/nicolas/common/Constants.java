/*
 * Constants.java
 *
 */

package com.zhang.nicolas.common;

/**
 *
 */
public final class Constants {

    /** Creates a new instance of Constants */
    public Constants() {
    }

    public static final String SDCONFIG_KEY = "SDCONFIGKEY";

    public static final String DATASOURCE_KEY = "SDDATASOURCEKEY";

    public static final String SESSION_USER_OBJ_KEY = "SDSESSIONUSEROBJKEY";

    public static final String COOKIE_USER_NAME = "SDCOOKIEUSERNAME";

	public static final String COOKIE_USER_COLOR = "SDCOOKIEUSERCOLOR";
	
	
	
	//================== User color schemes ==========================
	public static final String USER_COLOR_SCHEME_GRAY = "gray";
}

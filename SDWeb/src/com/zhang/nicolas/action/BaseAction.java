/*
 * BaseAction
 *
 */

package com.zhang.nicolas.action;

import javax.servlet.http.*;
import org.apache.struts.action.*;
import org.apache.commons.logging.*;

import com.zhang.nicolas.common.*;
import com.zhang.nicolas.entity.*;

/**
 *
 */
public class BaseAction extends Action {

    /** Creates a new instance of BaseAction */
    public BaseAction() {
    }

    /*
     * Get a log instance
     */
    private Log log = LogFactory.getLog(BaseAction.class);
    
    /*
     * Session user object, widely used by every action
     */
    public SessionUserObject suo = null;
    
    /*
     * execute function
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
        						 HttpServletResponse response) throws Exception {
        WebToolKit kit = WebToolKit.getInstance();
        HttpSession session = request.getSession(true);
        if (session == null) {
        	log.warn("Get session from request failed, ignore next logics.");
        	return null;
        }
        
        // check out user session object
        suo = (SessionUserObject)(session.getAttribute(Constants.SESSION_USER_OBJ_KEY));
        if (suo == null) {
        	// session user object hasn't been created yet, create it
        	suo = new SessionUserObject();
        	session.setAttribute(Constants.SESSION_USER_OBJ_KEY, suo);
        }
        
        // Check whether user has logined
        if (suo.getUsername() == null) {
        	String username = kit.CheckCookies(request, Constants.COOKIE_USER_NAME);
	        if (username != null) {
	        	suo.setUsername(username);
	        }
        }
        
        // Check user prefer color
        if (suo.getUsercolor() == null) {
        	String usercolor = kit.CheckCookies(request, Constants.COOKIE_USER_COLOR);
        	if (usercolor != null) {
        		suo.setUsercolor(usercolor);
        	} else {
        		// no user color found in session & cookie
        		// so we set a default "gray" color to user
        		suo.setUsercolor(Constants.USER_COLOR_SCHEME_GRAY);
        	}
        }
        
        // always return null
        return null;
    }
}
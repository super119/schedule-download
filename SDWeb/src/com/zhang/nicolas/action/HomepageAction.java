/*
 * HomepageAction
 *
 */

package com.zhang.nicolas.action;

import javax.servlet.http.*;
import org.apache.struts.action.*;
import org.apache.commons.logging.*;

import com.zhang.nicolas.common.*;
import com.zhang.nicolas.logic.*;
import com.zhang.nicolas.entity.*;

/**
 *
 */
public class HomepageAction extends BaseAction {

    /** Creates a new instance of HomepageAction */
    public HomepageAction() {
    }

    /*
     * Get a log instance
     */
    private Log log = LogFactory.getLog(HomepageAction.class);

    /*
     * execute function
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
        						 HttpServletResponse response) throws Exception {
        						 	
        // call parent's execute, handle user login, color prefer stuffs
        super.execute(mapping, form, request, response);

        // Call logic function
        HomepageBean hpb = new HomepageBean();
        SDConfig sdconfig = (SDConfig)this.getServlet().getServletContext().getAttribute(Constants.SDCONFIG_KEY);
        hpb.setSdconfig(sdconfig);
        hpb.setDatasource(this.getServlet().getServletContext().getAttribute(Constants.DATASOURCE_KEY));

		// username can be NULL if user hasn't login in
        String return_value = hpb.getHomePageInformation(suo.getUsername());
        if (return_value != null) {
            log.error("<execute>: HomepageAction Failed! Return value from HomepageBean is: " + return_value);
            ActionMessages errors = new ActionMessages();
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage(return_value));
            this.saveErrors(request, errors);
            return mapping.findForward("ErrorPage");
        }

        // Success
        // request.setAttribute("RESLIST", hpb.resList);
        // if (username != null)
        //     request.setAttribute("USERDOWNINFO", hpb.userDowns);

        return mapping.findForward("indexpage");
    }
}
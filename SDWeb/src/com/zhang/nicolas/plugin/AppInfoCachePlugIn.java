/*
 * AppInfoCachePlugIn.java
 *
 */
package com.zhang.nicolas.plugin;

import javax.servlet.ServletException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.PlugIn;
import org.apache.struts.config.ModuleConfig;
import com.zhang.nicolas.common.Constants;
import com.zhang.nicolas.common.SDConfig;
import java.io.*;
import java.util.Properties;
import javax.naming.*;
import javax.sql.*;


public final class AppInfoCachePlugIn implements PlugIn {


    // ----------------------------------------------------- Instance Variables

    /**
     * Logging output for this plug in instance.
     */
    private Log log = LogFactory.getLog(AppInfoCachePlugIn.class);

    /**
     * The {@link ActionServlet} owning this application.
     */
    private ActionServlet servlet = null;


    // ------------------------------------------------------------- Properties


    /**
     * The web application configfile path, default value
     */
    private String pathname = "sd.properties";

    public String getPathname() {
        return (this.pathname);
    }

    public void setPathname(String pathname) {
        this.pathname = pathname;
    }


    // --------------------------------------------------------- PlugIn Methods


    /**
     * Gracefully releasing any resources
     * that were allocated at initialization.
     */
    public void destroy() {

        log.info("<destroy>: Removing SDCONFIG and DATASOURCE instances from servlet context......");
        servlet.getServletContext().removeAttribute(Constants.SDCONFIG_KEY);
        servlet.getServletContext().removeAttribute(Constants.DATASOURCE_KEY);
        servlet = null;
    }


    /**
     * Initialize and read config info
     *
     * @param servlet The ActionServlet for this web application
     * @param config The ApplicationConfig for our owning module
     *
     * @exception ServletException if we cannot configure ourselves correctly
     */
    public void init(ActionServlet servlet, ModuleConfig config)
        throws ServletException {

        log.info("<init>: Entering init function......");
        log.info("<init>: Reading ConfigFile - '" + pathname + "'");

        // Remember our associated configuration and servlet
        this.servlet = servlet;

        // Looking for properties file
        InputStream is = null;
        Properties props = new Properties();

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        if (classLoader == null) {
            classLoader = this.getClass().getClassLoader();
        }

        is = classLoader.getResourceAsStream(pathname);
        if (is != null) {
            try {
                props.load(is);
            } catch (IOException e) {
                log.error("<init>: Load Properties file failed", e);
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    log.error("<init>: Close InputStream at finally block failed", e);
                }
            }
        }

        SDConfig sdconfig = new SDConfig();
        sdconfig.setDataSourceName(props.getProperty("DataSourceName"));
        log.info("<init>: Reading config, DataSourceName is " + sdconfig.getDataSourceName());
        sdconfig.setWebContextName(props.getProperty("WebContextName"));
        log.info("<init>: Reading config, WebContextName is " + sdconfig.getWebContextName());

        // Cache the JNDI Lookup result
        String context_name = "java:comp/env/" + sdconfig.getDataSourceName();
        log.info("<init>: Looking up DataSource context, the context string is: " + context_name);

        DataSource ds = null;
        try {
            Context ctx = new InitialContext();
            if(ctx == null) {
                log.error("<init>: JNDI Operation: New InitialContext Failed!");
                throw new ServletException("JNDI Operation: New InitialContext Failed!");
            }

            ds = (DataSource)ctx.lookup(context_name);
            servlet.getServletContext().setAttribute(Constants.DATASOURCE_KEY, ds);
        } catch (NamingException e1) {
            log.error("<init>: Looking up context -  " + context_name + " failed ", e1);
            throw new ServletException("Looking up context -  " + context_name + " failed!");
        }
        log.info("<init>: Caching DataSource Instance success.");

        // Make the initialized database available
        servlet.getServletContext().setAttribute(Constants.SDCONFIG_KEY, sdconfig);

        log.info("<init>: Leaving init function......");
    }


    // --------------------------------------------------------- Public Methods


    // ------------------------------------------------------ Protected Methods


    // -------------------------------------------------------- Private Methods


}
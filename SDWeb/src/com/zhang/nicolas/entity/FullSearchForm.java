/*
 * FullSearchForm.java
 *
 */

package com.zhang.nicolas.entity;

import org.apache.struts.action.*;
import javax.servlet.http.*;
import org.apache.commons.logging.*;

import com.zhang.nicolas.common.*;

/**
 *
 * @author Eric
 */
public class FullSearchForm extends ActionForm {
    
    /** Creates a new instance of FullSearchForm */
    public FullSearchForm() {
        super();
    }
    
    /*
     * Get a log instance
     */
    private Log log = LogFactory.getLog(FullSearchForm.class);
    
    private String searchKey = null;
    
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        log.debug("<validate>: Entering validate function......");
        
        WebToolKit kit = WebToolKit.getInstance();
        ActionErrors errors = new ActionErrors();
        
        // searchKey must be filled
        if (searchKey == null || searchKey.trim().length() < 1) {
            log.error("<validate>: searchKey is null...");
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("form.fullsearch.searchkey.null"));
        }
        
        log.debug("<validate>: Leaving validate function......");
        return errors;
    }
    
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        log.debug("<reset>: Entering reset function......");
        searchKey = null;
        log.debug("<reset>: Leaving reset function......");
    }

    public String getSearchKey() {
    	return searchKey;
    }
    public void setSearchKey(String value) {
    	searchKey = value;
    }
    
}
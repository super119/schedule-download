/*
 * Session user object bean
 */

package com.zhang.nicolas.entity;

public class SessionUserObject {
	String username = null;
	String usercolor = null;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String value) {
		username = value;
	}
	
	public String getUsercolor() {
		return usercolor;
	}
	public void setUsercolor(String value) {
		usercolor = value;
	}
}

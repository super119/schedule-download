/*
 * Schedule Download UI Thread
 * This thread is in charge of all UI stuffs
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#include "..\include\uithread.h"
#include "..\resource.h"
#include "..\include\utils.h"
#include "..\include\textres.h"

void init_login_dialog_ui(HWND hwnd)
{
	// set dialog caption
	SetWindowText(hwnd, GETTEXT("uithread.login.caption"));
	// set control text
	SetWindowText(GetDlgItem(hwnd, IDC_LOGIN_UN), GETTEXT("uithread.login.username"));
	SetWindowText(GetDlgItem(hwnd, IDC_LOGIN_PW), GETTEXT("uithread.login.password"));
	SetWindowText(GetDlgItem(hwnd, IDC_LOGIN_RE), GETTEXT("uithread.login.register"));
	SetWindowText(GetDlgItem(hwnd, IDC_LOGIN_FP), GETTEXT("uithread.login.forgetpass"));
	SetWindowText(GetDlgItem(hwnd, IDC_LOGIN_RP), GETTEXT("uithread.login.rempasswd"));
	SetWindowText(GetDlgItem(hwnd, IDC_LOGIN_AL), GETTEXT("uithread.login.autologin"));
	SetWindowText(GetDlgItem(hwnd, IDC_LOGIN_BTN), GETTEXT("uithread.login.button"));
	// set dialog icon
	SendMessage(hwnd, WM_SETICON, ICON_SMALL, (LPARAM)LoadIcon(GetModuleHandle(NULL), 
				MAKEINTRESOURCE(IDI_ICON1)));
	SendMessage(hwnd, WM_SETICON, ICON_BIG, (LPARAM)LoadIcon(GetModuleHandle(NULL), 
				MAKEINTRESOURCE(IDI_ICON1)));
	// center the dialog
	int screen_width = GetSystemMetrics(SM_CXSCREEN);
	int screen_height = GetSystemMetrics(SM_CYSCREEN);
	RECT dialog_rect;
	GetWindowRect(hwnd, &dialog_rect);
	if (screen_width >0 && screen_height > 0 &&
		(dialog_rect.bottom - dialog_rect.top) > 0 && 
		(dialog_rect.right - dialog_rect.left) > 0) {
			SetWindowPos(hwnd, NULL, (screen_width - dialog_rect.right + dialog_rect.left) / 2, 
				(screen_height - dialog_rect.bottom + dialog_rect.top) / 2, 0, 0, 
				SWP_NOZORDER | SWP_NOSIZE);
	}
}

BOOL CALLBACK login_dialog_proc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	switch(Message)
	{
		case WM_INITDIALOG:
			// Init dialog interface
			init_login_dialog_ui(hwnd);
			break;
		case WM_CTLCOLORSTATIC:
		{
			//wParam:HDC,   lParam:Handle of Static
			// check whether the static we cared about
			HWND static_handle = (HWND)lParam;
			if (static_handle == GetDlgItem(hwnd, IDC_LOGIN_RE) || 
				static_handle == GetDlgItem(hwnd, IDC_LOGIN_FP)) {
				SetTextColor((HDC)wParam, RGB(0, 0, 255));
				SetBkMode((HDC)wParam, TRANSPARENT);
				return((BOOL)GetStockObject(HOLLOW_BRUSH));
			}
			break;
		}
		case WM_MOUSEMOVE:
		{
			RECT re_rect, fp_rect;
			POINT Point;
			GetWindowRect(GetDlgItem(hwnd, IDC_LOGIN_RE), &re_rect);
			GetWindowRect(GetDlgItem(hwnd, IDC_LOGIN_FP), &fp_rect);
			GetCursorPos(&Point);   
			if(PtInRect(&re_rect, Point) || PtInRect(&fp_rect, Point)) {
				SetCursor(LoadCursor(NULL, IDC_HAND));
				return TRUE;
			} else {
				return FALSE;
			}
		}
		case WM_CLOSE:
			EndDialog(hwnd, IDCANCEL);
			break;
		default:
			return FALSE;
	}
	return TRUE;
}

unsigned ui_thread_init(void *pArgs)
{
	int login_dlg_ret;

	// Display login dialog
	login_dlg_ret = DialogBox(GetModuleHandle(NULL), 
		MAKEINTRESOURCE(IDD_DIALOG_LOGIN), NULL, login_dialog_proc);
	if (login_dlg_ret == IDOK) {
		MessageBox(NULL, TEXT("Login dialog confirmed."), TEXT("Info"), MB_OK | MB_ICONINFORMATION);
	} else {
		MessageBox(NULL, TEXT("Login dialog canceled."), TEXT("Info"), MB_OK | MB_ICONINFORMATION);
	}

	// Create main window

	return 0;
}

/*
 * Schedule Download Client main file
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#include <windows.h>
#include <tchar.h>
#include <strsafe.h>
#include "..\include\textres.h"
#include "..\include\utils.h"
#include "..\include\texts.h"
#include "..\include\logger.h"
#include "..\include\uithread.h"

#define OPERATION_FAILED	1
#define OPERATION_SUCCESS	0

int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
				     LPTSTR lpCmdLine, int nShowCmd)
{
	PTSTR error_msg;
	BOOL ret;
	TCHAR msg_text[1024];
	// program installed directory
	TCHAR root_directory[MAX_PATH];
	// thread handle & thread id array
	// uithread, jobfetch, jobhandle
	HANDLE hThreads[3];
	unsigned dwThreads[3];

	// Find out program root directory
	ret = utils_find_out_root_directory(root_directory, &error_msg);
	if (!ret) {
		if (error_msg != NULL &&
			SUCCEEDED(StringCchPrintf(msg_text, _countof(msg_text), MAIN_GET_ROOT_DIR_FAILED_VERBOSE, *error_msg))) {
			MessageBox(NULL, msg_text, MAIN_ERROR_MSG_BOX_CAPTION, MB_OK | MB_ICONERROR);
		} else {
			MessageBox(NULL, MAIN_GET_ROOT_DIR_FAILED, MAIN_ERROR_MSG_BOX_CAPTION, MB_OK | MB_ICONERROR);
		}
		if (error_msg != NULL) LocalFree(error_msg);
		return OPERATION_FAILED;
	}

#ifdef _DEBUG
	// make logger works
	ret = logger_set_root_directory(root_directory);
	if (!ret) {
		MessageBox(NULL, MAIN_LOGGER_SET_ROOTDIR_FAILED, MAIN_ERROR_MSG_BOX_CAPTION, MB_OK | MB_ICONERROR);
		return OPERATION_FAILED;
	}
#endif

	// Init text resources
	ret = text_res_set_root_directory(root_directory);
	if (!ret) {
		MessageBox(NULL, MAIN_INIT_TEXT_RES_FAILED, MAIN_ERROR_MSG_BOX_CAPTION, MB_OK | MB_ICONERROR);
		return OPERATION_FAILED;
	}
	ret = text_res_init();
	if (!ret) {
		MessageBox(NULL, MAIN_LOAD_TEXT_RES_FAILED, MAIN_ERROR_MSG_BOX_CAPTION, MB_OK | MB_ICONERROR);
		return OPERATION_FAILED;
	}

	// OK, start threads. Let's roll
	hThreads[0] = SDBEGINTHREADEX(NULL, 0, ui_thread_init, NULL, 0, &(dwThreads[0]));



	// Wait ui thread finish. Final we should wait for multiple threads to finish
	WaitForSingleObject(hThreads[0], INFINITE);

	// at the very end, release the text resources
	if (!text_res_finalize()) {
		LOGGER(LOG_LEVEL_WARNING, __SDFILE__, __LINE__, MAIN_RELEASE_TEXT_RES_FAILED);
	}

	return OPERATION_SUCCESS;
}
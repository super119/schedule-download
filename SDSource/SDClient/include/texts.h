/*
 * Schedule Download Texts
 * This header file includes text strings which can't be involved
 * in textres module. Mostly these strings are used before textres
 * module has initialized.
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#ifndef _TEXTS_H
#define _TEXTS_H

#define MAIN_GET_ROOT_DIR_FAILED_VERBOSE	TEXT("Find out SDClient root directory failed. \n\nReason: %s")
#define MAIN_GET_ROOT_DIR_FAILED			TEXT("Find out SDClient root directory failed")
#define MAIN_ERROR_MSG_BOX_CAPTION			TEXT("Error")
#define	MAIN_LOAD_TEXT_RES_FAILED			TEXT("Load text resources failed")
#define	MAIN_LOGGER_SET_ROOTDIR_FAILED		TEXT("Initialize logger failed")
#define MAIN_INIT_TEXT_RES_FAILED			TEXT("Initialize text resource module failed. \n\nThe text resource file can't be found.")
#define MAIN_RELEASE_TEXT_RES_FAILED		TEXT("Call text_res_finalize failed.")

#endif
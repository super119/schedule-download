/*
 * Schedule Download UI Thread
 * This thread is in charge of all UI stuffs
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#ifndef _UI_THREAD_H
#define _UI_THREAD_H

#include <windows.h>
#include <tchar.h>

/*
 * Construct and init the UI
 */
unsigned ui_thread_init(void *pArgs);


#endif
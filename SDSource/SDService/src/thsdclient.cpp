/*
 * Schedule Download SDClient listening thread
 * Event and Memory Map file is used as IPC between this thread and SDClient program
 * So SDClient should make sure there is only one instance is running
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */


#include <sdcommon.h>
#include "..\include\thsdclient.h"
#include "..\include\macros.h"
#include "..\include\defs.h"

#define THREAD_ERROR	1
#define THREAD_OK		0

// thread termination flag
BOOL thread_terminate = FALSE;

// Thread function
DWORD WINAPI thread_sdclient_listen(PVOID pvParam)
{
	HANDLE server_event;
	HANDLE sdclient_event = NULL;
	HANDLE sync_mutex;
	HANDLE mmap_file;
	DWORD errcode;
	DWORD thread_result = THREAD_OK;

	// Create server event first, this is a auto-reset event
	server_event = CreateEvent(NULL, FALSE, FALSE, THSDCLIENT_SERVER_EVENT_NAME);
	if (server_event == NULL) {
		GET_ERROR_CODE(errcode);
		M_RVIF_WITH_LOG(FALSE, THREAD_ERROR, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, 
			TEXT("SDClient listening thread create server event failed. Reason: %s"), sdc_utils_format_error_string(errcode));
	}

	// Create sync mutex, this is used between this thread and SDClient
	sync_mutex = CreateMutex(NULL, FALSE, THSDCLIENT_SYNC_MUTEX_NAME);
	if (sync_mutex == NULL) {
		GET_ERROR_CODE(errcode);
		M_RVIF_WITH_LOG(FALSE, THREAD_ERROR, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, 
			TEXT("SDClient listening thread create sync mutex failed. Reason: %s"), sdc_utils_format_error_string(errcode));
	}

	// Create unnamed memory map file to communicate with sdclient
	mmap_file = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, 32 * MEGA_BYTES, THSDCLIENT_MMAP_FILE_NAME);
	if (mmap_file == NULL) {
		GET_ERROR_CODE(errcode);
		M_RVIF_WITH_LOG(FALSE, THREAD_ERROR, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, 
			TEXT("SDClient listening thread create memory map file failed. Reason: %s"), sdc_utils_format_error_string(errcode));
	}

	// begin work
	// TODO: SDService main thread notify terminate
	while (TRUE) {
		// wait command from SDClient
		WaitForSingleObject(server_event, INFINITE);
		// get signal from sdclient, get the lock and begin work
		WaitForSingleObject(sync_mutex, INFINITE);
		// read memory map file
		PVOID mmap_view = MapViewOfFile(mmap_file, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, 0);
		if (mmap_view != NULL) {
			// TODO, read command and fill results

			UnmapViewOfFile(mmap_view);
		}

		ReleaseMutex(sync_mutex);
		// notify sdclient done
		if (sdclient_event == NULL) {
			sdclient_event = OpenEvent(EVENT_MODIFY_STATE, FALSE, THSDCLIENT_CLIENT_EVENT_NAME);
			if (sdclient_event == NULL) {
				GET_ERROR_CODE(errcode);
				LOGGER(LOG_LEVEL_ERROR, __SDFILE__, __LINE__, TEXT("Get sdclient event failed. SDClient listening thread quit. Reason: %s"), 
					sdc_utils_format_error_string(errcode));
				thread_result = THREAD_ERROR;
				break;
			}
		}
		if (!SetEvent(sdclient_event)) {
			GET_ERROR_CODE(errcode);
			LOGGER(LOG_LEVEL_ERROR, __SDFILE__, __LINE__, TEXT("Signal sdclient event failed. SDClient listening thread quit. Reason: %s"), 
				sdc_utils_format_error_string(errcode));
			thread_result = THREAD_ERROR;
			break;
		}

	}

	return thread_result;
}
/*
 * Schedule Download service program
 * SDService is in charge of job fetching, job working, job monitoring works
 * In addition, it also provides job informations to SDClient
 * We made it as a windows services because only windows service
 * runs when windows starting up(before user login).
 *
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#include <windows.h>
#include <tchar.h>
#include <sdcommon.h>
#include <process.h>
#include "..\include\macros.h"
#include "..\include\defs.h"
#include "..\include\thsdclient.h"
#include <strsafe.h>

#define OPERATION_SUCCESS	0
#define	OPERATION_FAILED	1

static BOOL user_auth(PCTSTR username, PCTSTR password)
{
	HINTERNET sess, conn, req;
	char buffer[HTTP_RECV_SBUF_SIZE];
	DWORD data_len = 0;
	TCHAR auth_url[HTTP_URL_MAX_LEN];

	sess = conn = req = NULL;
	M_RETURN_VAL_IF_FAIL(username != NULL, FALSE);
	M_RETURN_VAL_IF_FAIL(password != NULL, FALSE);
	
	M_RVIF_WITH_LOG(SUCCEEDED(StringCchPrintf(auth_url, _countof(auth_url), 
		TEXT("%s/UserAuth.sd?user=%s&pass=%s"), SD_WEBSITE, username, password)), FALSE, 
		LOG_LEVEL_ERROR, __SDFILE__, __LINE__, TEXT("Create user authentication url failed."));

	M_GTIF_WITH_LOG(sdc_http_get_text(auth_url, &sess, &conn, &req), failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, 
		TEXT("User authentication failed because of http operation errors."));

	
	sdc_http_close(&sess, &conn, &req);
	return TRUE;
failed:
	sdc_http_close(&sess, &conn, &req);
	return FALSE;
}

int _tmain(int argc, TCHAR *argv[], TCHAR *env[])
{
	// singleton mutex
	HANDLE singleton_mutex;

	// Threads
	HANDLE sdclient_thread, jobfetch_thread, jobcreate_thread, jobmon_thread, joboperate_thread;
	DWORD sdclient_thread_id, jobfetch_thread_id, jobcreate_thread_id, jobmon_thread_id, joboperate_thread_id;

	// Other variables
	TCHAR root_dir[MAX_PATH];
	DWORD errcode;
	TCHAR sd_username[REGISTRY_VALUE_SIZE];
	TCHAR sd_password[REGISTRY_VALUE_SIZE];
	TCHAR adsl_phone_book[REGISTRY_VALUE_SIZE];
	TCHAR adsl_entry_name[REGISTRY_VALUE_SIZE];
	TCHAR adsl_enable[REGISTRY_VALUE_SIZE];
	BOOL blocking = FALSE;
	HRASCONN adsl_conn = NULL;

	// find out root directory first
	M_RETURN_VAL_IF_FAIL(sdc_utils_find_root_directory(root_dir), OPERATION_FAILED);

	// init logger
	M_RETURN_VAL_IF_FAIL(sdc_logger_enable(root_dir, TEXT("servlog")), OPERATION_FAILED);

	// whether the SDService has started? singleton check
	singleton_mutex = CreateMutex(NULL, FALSE, SDSERVICE_SINGLETON_ID);
	if (singleton_mutex == NULL) {
		GET_ERROR_CODE(errcode);
		M_RVIF_WITH_LOG(FALSE, OPERATION_FAILED, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, 
			TEXT("Create singleton mutex failed. Reason: %s"), sdc_utils_format_error_string(errcode));
	}
	if (GetLastError() == ERROR_ALREADY_EXISTS) {
		// one sdservice instance has started, quit
		M_RVIF_WITH_LOG(FALSE, OPERATION_FAILED, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, TEXT("One SDService has started. Quit..."));
	}

	// create SDClient listening thread
	sdclient_thread = SDBEGINTHREADEX(NULL, 0, thread_sdclient_listen, NULL, 0, &sdclient_thread_id);
	if (sdclient_thread <= 0) {
		GET_ERROR_CODE(errcode);
		M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, 
			TEXT("Create sdclient listening thread failed. Reason: %s"), sdc_utils_format_error_string(errcode));
	}
	LOGGER(LOG_LEVEL_INFO, __SDFILE__, __LINE__, TEXT("SDClient listening thread started..."));

	// get sd username and password
	do {
		// read username & password from registry
		SecureZeroMemory(sd_username, sizeof(sd_username));
		SecureZeroMemory(sd_password, sizeof(sd_password));
		sdc_utils_get_registry_value(SD_REGISTRY_KEY, SDUSER_REGISTRY_ENTRY, sd_username, sizeof(sd_username));
		sdc_utils_get_registry_value(SD_REGISTRY_KEY, SDPASS_REGISTRY_ENTRY, sd_password, sizeof(sd_password));

		// if username & password aren't set, we can't do anything here
		if (sdc_utils_is_string_empty(sd_username) || sdc_utils_is_string_empty(sd_password)) {
			if (!blocking) {
				LOGGER(LOG_LEVEL_WARNING, __SDFILE__, __LINE__, TEXT("Username or password can't be found. Service blocking now."));
			}
			blocking = TRUE;
			Sleep(60000);  // sleep 1 minutes then try again
		} else {
			blocking = FALSE;
		}
	} while (blocking);
	LOGGER(LOG_LEVEL_INFO, __SDFILE__, __LINE__, TEXT("Got username %s and password %s, pretty good."));

	// check whether the network is ready
	do {
		if (!sdc_http_check_internet(TEXT("http://www.google.cn")) && !sdc_http_check_internet(TEXT("http://www.126.com"))) {
			// network is not available, adsl dialing enable?
			SecureZeroMemory(adsl_enable, sizeof(adsl_enable));
			sdc_utils_get_registry_value(SD_REGISTRY_KEY, ADSL_ENABLE_REGISTRY_ENTRY, adsl_enable, sizeof(adsl_enable));
			if (sdc_utils_is_string_empty(adsl_enable) || _tcscmp(adsl_enable, TEXT("Y")) != 0) {
				// there is no way to open the internet, the only thing we can do here is wait
				if (blocking == FALSE) {
					LOGGER(LOG_LEVEL_WARNING, __SDFILE__, __LINE__, 
						TEXT("Internet connection isn't available and the adsl dialing is not enabled. Service blocking..."));
				}
				blocking = TRUE;
			} else {
				// get adsl phonebook and entry
				SecureZeroMemory(adsl_phone_book, sizeof(adsl_phone_book));
				SecureZeroMemory(adsl_entry_name, sizeof(adsl_entry_name));
				sdc_utils_get_registry_value(SD_REGISTRY_KEY, ADSL_PHONE_BOOK_REGISTRY_ENTRY, adsl_phone_book, sizeof(adsl_phone_book));
				sdc_utils_get_registry_value(SD_REGISTRY_KEY, ADSL_ENTRY_REGISTRY_ENTRY, adsl_entry_name, sizeof(adsl_entry_name));

				if (sdc_utils_is_string_empty(adsl_phone_book) || sdc_utils_is_string_empty(adsl_entry_name)) {
					// can't get adsl phonebook and entry, can't dial
					if (blocking == FALSE) {
						LOGGER(LOG_LEVEL_WARNING, __SDFILE__, __LINE__, 
							TEXT("Internet connection isn't available. Try to dial the adsl but the phonebook/entry is absent. Service blocking..."));
					}
					blocking = TRUE;
				} else {
					// OK, begin dial the ADSL
					if (!sdc_adsl_dial(adsl_phone_book, adsl_entry_name, &adsl_conn)) {
						if (blocking == FALSE) {
							LOGGER(LOG_LEVEL_WARNING, __SDFILE__, __LINE__, 
								TEXT("Internet connection isn't available. Try to dial the adsl failed. Phonebook path is %s, Entry name is %s. Service blocking..."), 
								adsl_phone_book, adsl_entry_name);
						}
						blocking = TRUE;
					} else {
						blocking = FALSE;
						LOGGER(LOG_LEVEL_INFO, __SDFILE__, __LINE__, TEXT("Dial ADSL successfully, service continue."));
					}
				}
			}
			if (blocking) Sleep(60000);  // sleep 1 minute then try again
		} else {
			blocking = FALSE;
		}
	} while (blocking);
	LOGGER(LOG_LEVEL_INFO, __SDFILE__, __LINE__, TEXT("Internet connection is OK, good."));

	// internet is OK, now try to visit sd website. maybe our sd website is down temporarily
	do {
		if (!sdc_http_check_internet(SD_WEBSITE)) {
			// sd website down, wait
			if (blocking == FALSE) {
				LOGGER(LOG_LEVEL_ERROR, __SDFILE__, __LINE__, 
					TEXT("Can't connect to schedule download website. Service blocking..."));
			}
			blocking = TRUE;
		} else {
			blocking = FALSE;
		}
		if (blocking) Sleep(60000);
	} while (blocking);
	LOGGER(LOG_LEVEL_INFO, __SDFILE__, __LINE__, TEXT("Connection to schedule download website established, GOOD."));

	// user authentication
	do {
		if (!user_auth(sd_username, sd_password)) {
			// auth failed, blocking
			if (blocking == FALSE) {
				LOGGER(LOG_LEVEL_ERROR, __SDFILE__, __LINE__, TEXT("User authentication failed. Service blocking..."));
			}
			blocking = TRUE;
		} else {
			blocking = FALSE;
		}
		if (blocking) Sleep(60000);
	} while (blocking);
	LOGGER(LOG_LEVEL_INFO, __SDFILE__, __LINE__, TEXT("User authentication passed, start working..."));

	// everything is OK, spawn working threads
	jobfetch_thread = SDBEGINTHREADEX(NULL, 0, thread_job_fetch, NULL, 0, &jobfetch_thread_id);
	if (jobfetch_thread <= 0) {
		GET_ERROR_CODE(errcode);
		M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, 
			TEXT("Create job fetch thread failed. Reason: %s"), sdc_utils_format_error_string(errcode));
	}
	jobcreate_thread = SDBEGINTHREADEX(NULL, 0, thread_job_create, NULL, 0, &jobcreate_thread_id);
	if (jobcreate_thread <= 0) {
		GET_ERROR_CODE(errcode);
		M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, 
			TEXT("Create job create thread failed. Reason: %s"), sdc_utils_format_error_string(errcode));
	}
	jobmon_thread = SDBEGINTHREADEX(NULL, 0, thread_job_monitor, NULL, 0, &jobmon_thread_id);
	if (jobmon_thread <= 0) {
		GET_ERROR_CODE(errcode);
		M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, 
			TEXT("Create job monitoring thread failed. Reason: %s"), sdc_utils_format_error_string(errcode));
	}
	joboperate_thread = SDBEGINTHREADEX(NULL, 0, thread_job_operate, NULL, 0, &joboperate_thread_id);
	if (joboperate_thread <= 0) {
		GET_ERROR_CODE(errcode);
		M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, 
			TEXT("Create job operate thread failed. Reason: %s"), sdc_utils_format_error_string(errcode));
	}




failed:
	// first, terminate all threads
	

	// second, close all resources
	if (singleton_mutex != NULL)
		CloseHandle(singleton_mutex);
	if (adsl_conn != NULL)
		sdc_adsl_hangup(adsl_conn);
	return OPERATION_FAILED;
}
/*
 * Schedule Download Service Definitions
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#ifndef _DEFS_H
#define _DEFS_H

#define	MEGA_BYTES						1024 * 1024
#define HTTP_RECV_SBUF_SIZE				4096
// 256K stack buffer to save http response data
// if more data is available, write data into disk
#define HTTP_RECV_BBUF_SIZE				262144
#define HTTP_URL_MAX_LEN				256
#define SD_WEBSITE						TEXT("http://www.scheduledown.com")

#define REGISTRY_VALUE_SIZE				256
#define SD_REGISTRY_KEY					TEXT("Software\\nicolas.m.zhang@gmail.com\\sdconfig")
#define SDUSER_REGISTRY_ENTRY			TEXT("SDUser")
#define SDPASS_REGISTRY_ENTRY			TEXT("SDPass")
#define ADSL_ENABLE_REGISTRY_ENTRY		TEXT("AdslDialEnable")
#define ADSL_PHONE_BOOK_REGISTRY_ENTRY	TEXT("AdslPhoneBook")
#define ADSL_ENTRY_REGISTRY_ENTRY		TEXT("AdslEntry")

#define	SDSERVICE_SINGLETON_ID			TEXT("{CAAD5038-626F-4d79-A9BF-788514038639}")

#define THSDCLIENT_SERVER_EVENT_NAME	TEXT("{9753A1A3-43DB-4610-92C8-B0342EE53660}")
#define THSDCLIENT_SYNC_MUTEX_NAME		TEXT("{8C1ECCB4-80A7-4319-8853-FC79FF1FF316}")
#define	THSDCLIENT_MMAP_FILE_NAME		TEXT("{EDBB8CFD-9AD2-443a-BA91-0FE021641066}")
#define	THSDCLIENT_CLIENT_EVENT_NAME	TEXT("{55AB0033-7837-4fdc-A06F-B5F0574F2E01}")

#endif
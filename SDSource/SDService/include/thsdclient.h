/*
 * Schedule Download SDClient listening thread
 * Event and Memory Map file is used as IPC between this thread and SDClient program
 * So SDClient should make sure there is only one instance is running
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#ifndef _TH_SDCLIENT_H
#define _TH_SDCLIENT_H

#include <windows.h>

// Thread function
DWORD WINAPI thread_sdclient_listen(PVOID pvParam);



#endif
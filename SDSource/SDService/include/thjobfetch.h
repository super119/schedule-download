/*
 * Schedule Download Job fetching thread
 * Send HTTP requests to SD Website to get jobs need to process
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#ifndef _TH_JOB_FETCH_H
#define _TH_JOB_FETCH_H

#include <windows.h>

// Thread function
DWORD WINAPI thread_job_fetch(PVOID pvParam);



#endif
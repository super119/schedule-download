/*
 * Schedule Download Service Macros
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#ifndef _MACROS_H
#define _MACROS_H

//============================================= Common Macros
#define __SDFILE__		TEXT(__FILE__)

#define GET_ERROR_CODE(n)	\
	n = GetLastError();

#define M_RETURN_IF_FAIL(expr)	\
	if (!(expr)) return;

#define M_RETURN_VAL_IF_FAIL(expr, val) \
	if (!(expr)) return val;

#define M_GOTO_IF_FAIL(expr, label)	\
	if (!(expr)) goto label;

#define M_CONT_IF_FAIL(expr)	\
	if (!(expr)) continue;


#ifdef _DEBUG
#define LOGGER(log_level, filename, line, format, ...)	\
	sdc_logger_action(log_level, filename, line, format, __VA_ARGS__);
#else
#define LOGGER
#endif

#define M_RIF_WITH_LOG(expr, log_level, filename, line, format, ...)	\
	if (!(expr)) {	\
		LOGGER(log_level, filename, line, format, __VA_ARGS__);	\
		return;	\
	}

#define M_RVIF_WITH_LOG(expr, val, log_level, filename, line, format, ...)	\
	if (!(expr)) {	\
		LOGGER(log_level, filename, line, format, __VA_ARGS__);	\
		return val;	\
	}

#define M_GTIF_WITH_LOG(expr, label, log_level, filename, line, format, ...)	\
	if (!(expr)) {	\
		LOGGER(log_level, filename, line, format, __VA_ARGS__);	\
		goto label;	\
	}

typedef unsigned (__stdcall *PTHREAD_START) (void *);
#define SDBEGINTHREADEX(psa, cbStackSize, pfnStartAddr, pvParam, fdwCreateFlags, pdwThreadID)	\
      ((HANDLE) _beginthreadex(                     \
         (void *) (psa),                            \
         (unsigned) (cbStackSize),                  \
         (PTHREAD_START) (pfnStartAddr),            \
         (void *) (pvParam),                        \
         (unsigned) (fdwCreateFlags),                \
         (unsigned *) (pdwThreadID)))

#endif
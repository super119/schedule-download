/*
 * Schedule Download Http utilities, based on winhttp
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#include "..\include\http.h"
#include "..\include\logger.h"
#include "..\include\utils.h"
#include "..\include\defs.h"
#include "..\include\macros.h"
#include <strsafe.h>

// http server's hostname length
#define HTTP_HOST_NAME_MAX		255
#define HTTP_TEXT_MIMETYPE		1
#define HTTP_BIN_MIMETYPE		2

// mimetypes for WinHttpOpenRequest
LPCWSTR text_mimetype[] = { L"text/*", L"\0" };
LPCWSTR bin_mimetype[] = { L"application/*", L"audio/*", L"image/*", L"video/*", L"\0" };

/*
 * This function used internally. It checks the proxy settings and apply
 * to the WinHttp. Return winhttp session handle if succeeded, otherwise
 * return NULL.
 */
static HINTERNET open_winhttp()
{
	HINTERNET session = NULL;
	TCHAR proxy_enabled[REGISTRY_VALUE_SIZE];
	TCHAR proxy_server[REGISTRY_VALUE_SIZE];
	TCHAR proxy_port[REGISTRY_VALUE_SIZE];
	TCHAR proxy_username[REGISTRY_VALUE_SIZE];
	TCHAR proxy_password[REGISTRY_VALUE_SIZE];
	DWORD errcode;
	size_t proxy_username_len = 0;
	size_t proxy_password_len = 0;

	// Check whether the proxy has enabled
	SecureZeroMemory(proxy_enabled, sizeof(proxy_enabled));
	utils_get_registry_value(SD_REGISTRY_KEY, HTTP_PROXY_ENABLED_ENTRY, proxy_enabled, sizeof(proxy_enabled));
	if (utils_is_string_empty(proxy_enabled) || _tcscmp(proxy_enabled, TEXT("Y")) != 0) {
		// proxy isn't enabled, direct connect
		session = WinHttpOpen(HTTP_AGENT_NAME, WINHTTP_ACCESS_TYPE_NO_PROXY, WINHTTP_NO_PROXY_NAME, WINHTTP_NO_PROXY_BYPASS, 0);
		if (session == NULL) {
			GET_ERROR_CODE(errcode);
			M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_NO_PROXY_OPEN_FAILED, utils_format_error_string(errcode));
		}
	} else {
		// proxy enabled, read server/port/user/pass next
		SecureZeroMemory(proxy_server, sizeof(proxy_server));
		SecureZeroMemory(proxy_port, sizeof(proxy_port));
		SecureZeroMemory(proxy_username, sizeof(proxy_username));
		SecureZeroMemory(proxy_password, sizeof(proxy_password));
		utils_get_registry_value(SD_REGISTRY_KEY, HTTP_PROXY_SERVER_ENTRY, proxy_server, sizeof(proxy_server));
		utils_get_registry_value(SD_REGISTRY_KEY, HTTP_PROXY_PORT_ENTRY, proxy_port, sizeof(proxy_port));
		utils_get_registry_value(SD_REGISTRY_KEY, HTTP_PROXY_USER_ENTRY, proxy_username, sizeof(proxy_username));
		utils_get_registry_value(SD_REGISTRY_KEY, HTTP_PROXY_PASS_ENTRY, proxy_password, sizeof(proxy_password));
		M_GTIF_WITH_LOG(!utils_is_string_empty(proxy_server), failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_PROXY_SERVER_ABSENT);
		M_GTIF_WITH_LOG(!utils_is_string_empty(proxy_port), failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_PROXY_PORT_ABSENT);
		M_GTIF_WITH_LOG(utils_is_string_int(proxy_port), failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_PROXY_PORT_ILLEGAL);
		// add port into server string
		M_GTIF_WITH_LOG(SUCCEEDED(StringCchPrintf(proxy_server, _countof(proxy_server), TEXT("%s:%s"), proxy_server, proxy_port)), failed, 
			LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_STRING_OPERATION_FAILED);

		// open
		session = WinHttpOpen(HTTP_AGENT_NAME, WINHTTP_ACCESS_TYPE_NAMED_PROXY, proxy_server, WINHTTP_NO_PROXY_BYPASS, 0);
		if (session == NULL) {
			GET_ERROR_CODE(errcode);
			M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_OPEN_WITH_PROXY_FAILED, utils_format_error_string(errcode));
		}

		// set proxy username & password
		if (!utils_is_string_empty(proxy_username) && !utils_is_string_empty(proxy_password)) {
			M_GTIF_WITH_LOG(SUCCEEDED(StringCchLength(proxy_username, _countof(proxy_username), &proxy_username_len)), failed, 
				LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_STRING_OPERATION_FAILED);
			M_GTIF_WITH_LOG(SUCCEEDED(StringCchLength(proxy_password, _countof(proxy_password), &proxy_password_len)), failed, 
				LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_STRING_OPERATION_FAILED);
			if (!WinHttpSetOption(session, WINHTTP_OPTION_PROXY_USERNAME, proxy_username, proxy_username_len)) {
				GET_ERROR_CODE(errcode);
				M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_SET_PROXY_USER_FAILED, utils_format_error_string(errcode));
			}
			if (!WinHttpSetOption(session, WINHTTP_OPTION_PROXY_PASSWORD, proxy_password, proxy_password_len)) {
				GET_ERROR_CODE(errcode);
				M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_SET_PROXY_PASS_FAILED, utils_format_error_string(errcode));
			}
		}
	}

	// set timeouts
	if (!WinHttpSetTimeouts(session, HTTP_RESOLVE_TO, HTTP_CONNECT_TO, HTTP_SEND_TO, HTTP_RECV_TO)) {
		GET_ERROR_CODE(errcode);
		M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_SET_TIMEOUTS_FAILED, utils_format_error_string(errcode));
	}

	return session;
failed:
	if (session != NULL) WinHttpCloseHandle(session);
	return NULL;
}

static BOOL http_get(PCTSTR url, HINTERNET *sess, HINTERNET *conn, HINTERNET *req, DWORD mimetype)
{
	HINTERNET session = NULL, connect = NULL, request = NULL;
	BOOL result = FALSE;
	DWORD errcode;
	URL_COMPONENTS url_comp;
	size_t url_len_input;
	TCHAR http_host_name[HTTP_HOST_NAME_MAX];
	int hostname_index;

	M_RETURN_VAL_IF_FAIL(url != NULL, FALSE);
	M_RETURN_VAL_IF_FAIL(sess != NULL, FALSE);
	M_RETURN_VAL_IF_FAIL(conn != NULL, FALSE);
	M_RETURN_VAL_IF_FAIL(req != NULL, FALSE);
	M_RETURN_VAL_IF_FAIL(logger_is_enabled(), FALSE);

	session = open_winhttp();
	M_RETURN_VAL_IF_FAIL(session != NULL, FALSE);  // log has been done in open_winhttp function

	// crack url input
	// Initialize the URL_COMPONENTS structure.
    SecureZeroMemory(&url_comp, sizeof(url_comp));
    url_comp.dwStructSize = sizeof(url_comp);

    // Set required component lengths to non-zero 
    // so that they are cracked.
    url_comp.dwSchemeLength    = -1;
    url_comp.dwHostNameLength  = -1;
    url_comp.dwUrlPathLength   = -1;
    url_comp.dwExtraInfoLength = -1;

	M_GTIF_WITH_LOG(SUCCEEDED(StringCchLength(url, STRSAFE_MAX_CCH, &url_len_input)), failed, LOG_LEVEL_ERROR, 
		__SDFILE__, __LINE__, HTTP_STRING_OPERATION_FAILED);
	result = WinHttpCrackUrl(url, url_len_input, 0, &url_comp);
	if (!result) {
		GET_ERROR_CODE(errcode);
		M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_CRACK_URL_FAILED, utils_format_error_string(errcode));
	}

	/* No need to create url because No winhttp functions accept a whole escaped url 
	// get new url length, allocate it
	WinHttpCreateUrl(&url_comp, 0, NULL, &url_len_cracked);
	M_GTIF_WITH_LOG(url_len_cracked > 0, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_CREATE_URL_GET_LEN_FAILED);
	// the length WinHttpCreateUrl generated is wide character numbers
	url_cracked = (TCHAR *)HeapAlloc(sdc_heap, HEAP_ZERO_MEMORY, url_len_cracked * sizeof(WCHAR));
	M_GTIF_WITH_LOG(url_cracked != NULL, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_HEAP_ALLOC_FAILED);
	// call create url again, this time get result url
	result = WinHttpCreateUrl(&url_comp, 0, url_cracked, &url_len_cracked);
	if (!result) {
		GET_ERROR_CODE(errcode);
		M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_CREATE_URL_FAILED, utils_format_error_string(errcode));
	} */

	// url OK, connect. Notice here WinHttpConnect needs Host/Server name, not the whole URL
	// Request file and parameters should be set in WinHttpOpenRequest function
	// get hostname
	hostname_index = utils_find_char(url_comp.lpszHostName, (TCHAR)'/');
	if (hostname_index < 0) {
		// not found or error occurs, use the original hostname
		connect = WinHttpConnect(session, url_comp.lpszHostName, INTERNET_DEFAULT_HTTP_PORT, 0);
	} else {
		// found "/"
		memcpy_s(http_host_name, sizeof(http_host_name), url_comp.lpszHostName, hostname_index * sizeof(TCHAR));
		http_host_name[hostname_index] = (TCHAR)'\0';
		connect = WinHttpConnect(session, http_host_name, INTERNET_DEFAULT_HTTP_PORT, 0);
	}
	if (connect == NULL) {
		GET_ERROR_CODE(errcode);
		M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_CONNECT_FAILED, utils_format_error_string(errcode));
	}

	if (mimetype == HTTP_TEXT_MIMETYPE) {
		request = WinHttpOpenRequest(connect, TEXT("GET"), url_comp.lpszUrlPath, NULL, WINHTTP_NO_REFERER, 
						text_mimetype, WINHTTP_FLAG_ESCAPE_PERCENT | WINHTTP_FLAG_REFRESH);
	} else {
		request = WinHttpOpenRequest(connect, TEXT("GET"), url_comp.lpszUrlPath, NULL, WINHTTP_NO_REFERER, 
						bin_mimetype, WINHTTP_FLAG_ESCAPE_PERCENT | WINHTTP_FLAG_REFRESH);
	}
	if (request == NULL) {
		GET_ERROR_CODE(errcode);
		M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_OPEN_REQUEST_FAILED, utils_format_error_string(errcode));
	}

    // Send the request
    result = WinHttpSendRequest(request, WINHTTP_NO_ADDITIONAL_HEADERS, 0, WINHTTP_NO_REQUEST_DATA, 0, 0, 0 );
	if (!result) {
		GET_ERROR_CODE(errcode);
		M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_SEND_REQUEST_FAILED, utils_format_error_string(errcode));
	}

	// Start receiving
	result = WinHttpReceiveResponse(request, NULL);
	if (!result) {
		GET_ERROR_CODE(errcode);
		M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_RECV_RESPONSE_FAILED, utils_format_error_string(errcode));
	}

	// OK, return
	*sess = session;
	*conn = connect;
	*req = request;
	return TRUE;

failed:
    // Close any open handles
    if (request) WinHttpCloseHandle(request);
    if (connect) WinHttpCloseHandle(connect);
    if (session) WinHttpCloseHandle(session);
	return FALSE;
}

BOOL http_get_text(PCTSTR url, HINTERNET *sess, HINTERNET *conn, HINTERNET *req)
{
	return http_get(url, sess, conn, req, HTTP_TEXT_MIMETYPE);
}

BOOL http_get_bin(PCTSTR url, HINTERNET *sess, HINTERNET *conn, HINTERNET *req)
{
	return http_get(url, sess, conn, req, HTTP_BIN_MIMETYPE);
}

BOOL http_check_data_len(HINTERNET request, DWORD *len)
{
	DWORD data_size = 0;
	BOOL result = FALSE;
	DWORD errcode;

	M_RETURN_VAL_IF_FAIL(request != NULL, FALSE);
	M_RETURN_VAL_IF_FAIL(len != NULL, FALSE);
	M_RETURN_VAL_IF_FAIL(logger_is_enabled(), FALSE);

	// Check the data length
	result = WinHttpQueryDataAvailable(request, &data_size);
	if (!result) {
		GET_ERROR_CODE(errcode);
		M_RVIF_WITH_LOG(FALSE, FALSE, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_QUERY_DATA_FAILED, utils_format_error_string(errcode));
	}

	*len = data_size;
	return TRUE;
}

BOOL http_recv(HINTERNET request, LPVOID buffer, DWORD buffer_len, DWORD *bytes_read)
{
	BOOL result = FALSE;
	DWORD errcode;

	M_RETURN_VAL_IF_FAIL(request != NULL, FALSE);
	M_RETURN_VAL_IF_FAIL(buffer != NULL, FALSE);
	M_RETURN_VAL_IF_FAIL(buffer_len > 0, FALSE);
	M_RETURN_VAL_IF_FAIL(bytes_read != NULL, FALSE);
	M_RETURN_VAL_IF_FAIL(logger_is_enabled(), FALSE);

	// Read the Data.
    SecureZeroMemory(buffer, buffer_len);
    result = WinHttpReadData(request, buffer, buffer_len, bytes_read);
	if (!result) {
		GET_ERROR_CODE(errcode);
		M_RVIF_WITH_LOG(FALSE, FALSE, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, HTTP_READ_DATA_FAILED, utils_format_error_string(errcode));
	}

	return TRUE;
}

void http_close(HINTERNET *session, HINTERNET *connect, HINTERNET *request)
{
	if (request != NULL) {
		if (*request) {
			WinHttpCloseHandle(*request);
			*request = NULL;
		}
	}

	if (connect != NULL) {
		if (*connect) {
			WinHttpCloseHandle(*connect);
			*connect = NULL;
		}
	}

	if (session != NULL) {
		if (*session) {
			WinHttpCloseHandle(*session);
			*session = NULL;
		}
	}

	return;
}

BOOL http_check_internet(PCTSTR url)
{
	HINTERNET session = NULL, connect = NULL, request = NULL;
	BOOL result = FALSE;
	DWORD data_len = 0;
	
	M_RETURN_VAL_IF_FAIL(url != NULL, FALSE);
	M_RETURN_VAL_IF_FAIL(logger_is_enabled(), FALSE);
	
	result = http_get_text(url, &session, &connect, &request);
	M_RETURN_VAL_IF_FAIL(result, FALSE); // logger has been done in http_send

	result = http_check_data_len(request, &data_len);
	M_RETURN_VAL_IF_FAIL(result, FALSE);
	M_RETURN_VAL_IF_FAIL(data_len > 0, FALSE);

	// internet is OK, no need to receive actual data
	http_close(&session, &connect, &request);

	return TRUE;
}
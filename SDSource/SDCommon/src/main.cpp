/*
 * Schedule Download Dll Main source
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#include <windows.h>
#include "..\include\defs.h"

/*
 * TLS Slot index, used for all threads in
 * utils_format_error_string
 */
DWORD tls_index_utils;
// this tls slot is allocated for adsl module
DWORD tls_index_adsl;

/*
 * Dll's own Heap
 */
HANDLE sdc_heap;

/*
 * Critical Section used to sync threads
 */
CRITICAL_SECTION sdc_cs_logger;
CRITICAL_SECTION sdc_cs_textres;
CRITICAL_SECTION sdc_cs_adsl;

// RAS dll module handle. For free this library when dll detach
extern HMODULE ras_module;

// DllMain entry
BOOL WINAPI DllMain(HINSTANCE hInstDll, DWORD fdwReason, PVOID fImpLoad)
{
	LPVOID error_string_buffer = NULL;
	LPVOID adsl_buffer = NULL;
	ULONG heap_frag_value = 2;

	switch (fdwReason) {
		case DLL_PROCESS_ATTACH:
			// init critical section
			InitializeCriticalSection(&sdc_cs_logger);
			InitializeCriticalSection(&sdc_cs_textres);
			InitializeCriticalSection(&sdc_cs_adsl);
			// Apply a TLS slot
			tls_index_utils = TlsAlloc();
			if (tls_index_utils == TLS_OUT_OF_INDEXES)
				return FALSE;
			tls_index_adsl = TlsAlloc();
			if (tls_index_adsl == TLS_OUT_OF_INDEXES)
				return FALSE;

			// create dll own heap
			sdc_heap = HeapCreate(NULL, MEGA_BYTES, 0);
			if (sdc_heap == NULL)
				return FALSE;
			// set low-fragmentation heap
			// we don't care about whether it failed
			HeapSetInformation(sdc_heap, HeapCompatibilityInformation,
                       &heap_frag_value, sizeof(heap_frag_value));
			// we don't break here, go on to next case to allocate
			// memory for this thread

		case DLL_THREAD_ATTACH:
			// new thread created, alloc memory
			// we don't care whether alloc success because utils_format_error_string
			// will check the second time
			error_string_buffer = HeapAlloc(sdc_heap, HEAP_ZERO_MEMORY, ERROR_STRING_BUFSIZE);
			TlsSetValue(tls_index_utils, error_string_buffer);
			// we don't alloc adsl buffer here because adsl module will alloc it when necessary
			// and adsl buffer is not a fixed size, not like error string in utils
			break;

		case DLL_THREAD_DETACH:
			// thread terminated
			error_string_buffer = TlsGetValue(tls_index_utils);
			if (error_string_buffer != NULL)
				HeapFree(sdc_heap, NULL, error_string_buffer);
			// free adsl buffer
			adsl_buffer = TlsGetValue(tls_index_adsl);
			if (adsl_buffer != NULL)
				HeapFree(sdc_heap, NULL, adsl_buffer);
			break;

		case DLL_PROCESS_DETACH:
			DeleteCriticalSection(&sdc_cs_logger);
			DeleteCriticalSection(&sdc_cs_textres);
			DeleteCriticalSection(&sdc_cs_adsl);

			if (ras_module != NULL) FreeLibrary(ras_module);

			// process terminating, we should free this thread's error string buffer
			// but instead we free the complete heap. Also the textres alloced memory is freed
			HeapDestroy(sdc_heap);

			// free Tls slot
			TlsFree(tls_index_utils);
			TlsFree(tls_index_adsl);
			break;
	}

	return TRUE; // Used only for DLL_PROCESS_ATTACH
}

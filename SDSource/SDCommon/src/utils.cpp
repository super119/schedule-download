/*
 * Schedule Download Utilities
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#include "..\include\utils.h"
#include "..\include\logger.h"
#include "..\include\defs.h"
#include "..\include\macros.h"
#include <strsafe.h>

extern DWORD tls_index_utils;
extern HANDLE sdc_heap;

PTSTR utils_format_error_string(const LONG code)
{
	DWORD systemLocale;
	BOOL ret;
	LPVOID error_string_buffer;
	size_t error_string_len;

	if (code < 0) {
		// illegal error code
		return NULL;
	}
	systemLocale = MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL);

	// get error string buffer
	error_string_buffer = TlsGetValue(tls_index_utils);
	if (error_string_buffer == NULL) {
		error_string_buffer = HeapAlloc(sdc_heap, HEAP_ZERO_MEMORY, ERROR_STRING_BUFSIZE);
		M_RETURN_VAL_IF_FAIL(error_string_buffer != NULL, NULL);
		M_GOTO_IF_FAIL(TlsSetValue(tls_index_utils, error_string_buffer) != NULL, tlsfailed);
	}

	// append the error code first
	M_RETURN_VAL_IF_FAIL(SUCCEEDED(StringCchPrintf((LPTSTR)error_string_buffer, ERROR_STRING_BUFSIZE / sizeof(TCHAR), TEXT("(%d)"), code)), NULL);

	// append error string then
	M_RETURN_VAL_IF_FAIL(SUCCEEDED(StringCchLength((LPTSTR)error_string_buffer, ERROR_STRING_BUFSIZE / sizeof(TCHAR), &error_string_len)), NULL);
	ret = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, 
						NULL, code, systemLocale, ((LPTSTR)error_string_buffer + error_string_len),
						ERROR_STRING_BUFSIZE / sizeof(TCHAR) - error_string_len, NULL);

	// we don't check the return value of FormatMessage because:
	// Some error code description saves in additional modules, normally in a dll, e.g: winhttp.dll
	// In this case, FormatMessage failed but the error_string_buffer contains the error code
	// so we also should return the string
	return (PTSTR)error_string_buffer;

tlsfailed:
	if (error_string_buffer != NULL)
		HeapFree(sdc_heap, NULL, error_string_buffer);
	return NULL;
}

BOOL utils_find_root_directory(PTSTR root_dir)
{
	HKEY reg_key;
	DWORD root_directory_len = MAX_PATH;
	LONG ret;

	// open the registry key
	ret = RegOpenKeyEx(HKEY_CURRENT_USER, SD_REGISTRY_KEY, 0, KEY_QUERY_VALUE, &reg_key);
	if (ret != ERROR_SUCCESS)
		goto regfailed;

	// query the key value
	ret = RegQueryValueEx(reg_key, UTILS_ROOT_DIRECTORY_KEY_ENTRY, NULL, NULL, 
						  (LPBYTE)root_dir, &root_directory_len);
	if (ret != ERROR_SUCCESS)
		goto regfailed;

	// get root directory success
	RegCloseKey(reg_key);
	return TRUE;

regfailed:
	if (reg_key != NULL) RegCloseKey(reg_key);
	// Get current working directory as root directory
	if(!GetCurrentDirectory(MAX_PATH, root_dir))
		return FALSE;
	return TRUE;
}

BOOL utils_file_exists(PCTSTR file_path)
{
	DWORD file_attr;

	M_RETURN_VAL_IF_FAIL(file_path != NULL, FALSE);
	file_attr = GetFileAttributes(file_path);
	if (file_attr == INVALID_FILE_ATTRIBUTES) return FALSE;
	if (file_attr & FILE_ATTRIBUTE_DIRECTORY) return FALSE;

	return TRUE;
}

BOOL utils_is_string_int(PCTSTR input)
{
	BOOL wide_string = TRUE;  // default is a wide string
	BYTE high_byte, low_byte;

	M_RETURN_VAL_IF_FAIL(!utils_is_string_empty(input), FALSE);
#ifdef UNICODE
	wide_string = TRUE;
#else
#ifdef _UNICODE
	wide_string = TRUE;
#else
	wide_string = FALSE;
#endif
#endif

	if (wide_string) {
		while (*input) {
			high_byte = HIBYTE(*input);
			low_byte = LOBYTE(*input);
			if ((high_byte == 0x00) && (low_byte > 47 && low_byte < 58))
				input++;
			else
				return FALSE;
		}
	} else {
		// ansi string
		while (*input) {
			if (*input > 47 && *input < 58)
				input++;
			else
				return FALSE;
		}
	}

	return TRUE;
}

BOOL utils_is_string_empty(PCTSTR input)
{
	M_RETURN_VAL_IF_FAIL(input != NULL, TRUE);
	while(*input) {
		if ((*input) == (TCHAR)' ')
			input++;
		else
			return FALSE;
	}

	return TRUE;
}

TCHAR *utils_trim_left(TCHAR *input)
{
	M_RETURN_VAL_IF_FAIL(input != NULL, NULL);

	while ((*input) == (TCHAR)' ')
		input++;

	return input;
}

TCHAR *utils_trim_right(TCHAR *input)
{
	size_t input_string_len = 0;
	TCHAR *tmp = NULL;

	M_RETURN_VAL_IF_FAIL(input != NULL, NULL);
	M_RETURN_VAL_IF_FAIL(SUCCEEDED(StringCchLength(input, STRSAFE_MAX_CCH, &input_string_len)), NULL);
	tmp = input + input_string_len - 1;
	
	while ((*tmp) == (TCHAR)' ') {
		*tmp = (TCHAR)'\0';
		tmp--;
	}

	// we just return the original pointer because we only modifies
	// the trailing part of the string
	// we defined a return value for user check whether the function succeeded
	return input;
}

TCHAR *utils_trim_both(TCHAR *input)
{
	TCHAR *tmp = NULL;
	M_RETURN_VAL_IF_FAIL(input != NULL, NULL);

	tmp = utils_trim_left(input);
	M_RETURN_VAL_IF_FAIL(tmp != NULL, NULL);

	tmp = utils_trim_right(tmp);
	M_RETURN_VAL_IF_FAIL(tmp != NULL, NULL);

	return tmp;
}

TCHAR *utils_remove_trailing_enter(TCHAR *input)
{
	size_t input_string_len = 0;
	TCHAR *tmp = NULL;

	M_RETURN_VAL_IF_FAIL(input != NULL, NULL);
	M_RETURN_VAL_IF_FAIL(SUCCEEDED(StringCchLength(input, STRSAFE_MAX_CCH, &input_string_len)), NULL);
	tmp = input + input_string_len - 1;
	
	while ((*tmp) == (TCHAR)'\r') {
		*tmp = (TCHAR)'\0';
		tmp--;
	}

	// we just return the original pointer because we only modifies
	// the trailing part of the string
	// we defined a return value for user check whether the function succeeded
	return input;
}

int utils_find_char(TCHAR *input, TCHAR key)
{
	int result = 0;

	M_RETURN_VAL_IF_FAIL(input != NULL, -2);
	
	while(*input) {
		if (*input == key)
			return result;
		result++;
		input++;
	}

	return -1;
}

BOOL utils_get_registry_value(PCTSTR key, PCTSTR entry, PTSTR value, DWORD valsize)
{
	HKEY reg_key;
	LONG ret;

	M_RETURN_VAL_IF_FAIL(!utils_is_string_empty(key), FALSE);
	M_RETURN_VAL_IF_FAIL(!utils_is_string_empty(entry), FALSE);
	M_RETURN_VAL_IF_FAIL(logger_is_enabled(), FALSE);

	// open the registry key
	ret = RegOpenKeyEx(HKEY_CURRENT_USER, key, 0, KEY_QUERY_VALUE, &reg_key);
	M_RVIF_WITH_LOG(ret == ERROR_SUCCESS, FALSE, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, 
		UTILS_OPEN_REG_KEY_FAILED, key, utils_format_error_string(ret));

	// query the key value
	ret = RegQueryValueEx(reg_key, entry, NULL, NULL, (LPBYTE)value, &valsize);
	M_RVIF_WITH_LOG(ret == ERROR_SUCCESS, FALSE, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, 
		UTILS_QUERY_REG_ENTRY_FAILED, entry, utils_format_error_string(ret));

	RegCloseKey(reg_key);
	return TRUE;
}

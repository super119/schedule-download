/*
 * Schedule Download SDCommon
 * This is just a wrapper for utils, logger... implementations
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#define SDCOMMONAPI extern "C" __declspec(dllexport)

#include "..\include\sdcommon.h"
#include "..\include\logger.h"
#include "..\include\textres.h"
#include "..\include\utils.h"
#include "..\include\http.h"
#include "..\include\adsl.h"

//======================================= Http module

BOOL sdc_http_check_internet(PCTSTR url)
{
	return http_check_internet(url);
}

BOOL sdc_http_get_text(PCTSTR url, HINTERNET *sess, HINTERNET *conn, HINTERNET *req)
{
	return http_get_text(url, sess, conn, req);
}

BOOL sdc_http_get_bin(PCTSTR url, HINTERNET *sess, HINTERNET *conn, HINTERNET *req)
{
	return http_get_bin(url, sess, conn, req);
}

BOOL sdc_http_check_data_len(HINTERNET request, DWORD *len)
{
	return http_check_data_len(request, len);
}

BOOL sdc_http_recv(HINTERNET request, LPVOID buffer, DWORD buffer_len, DWORD *bytes_read)
{
	return http_recv(request, buffer, buffer_len, bytes_read);
}

void sdc_http_close(HINTERNET *session, HINTERNET *connect, HINTERNET *request)
{
	http_close(session, connect, request);
	return;
}

//======================================= Adsl module

RASENTRYNAME *sdc_adsl_enum_entries(DWORD *entry_num)
{
	return adsl_enum_entries(entry_num);
}

WORD sdc_adsl_entry_save_password(PCTSTR phone_book_path, PCTSTR entry_name)
{
	return adsl_entry_save_password(phone_book_path, entry_name);
}

BOOL sdc_adsl_dial(PCTSTR phone_book_path, PCTSTR entry_name, HRASCONN *adsl_conn)
{
	return adsl_dial(phone_book_path, entry_name, adsl_conn);
}

BOOL sdc_adsl_hangup(HRASCONN adsl_conn)
{
	return adsl_hangup(adsl_conn);
}

//======================================= Utility module

PTSTR sdc_utils_format_error_string(const LONG code)
{
	return utils_format_error_string(code);
}

BOOL sdc_utils_find_root_directory(PTSTR root_dir)
{
	return utils_find_root_directory(root_dir);
}

BOOL sdc_utils_file_exists(PCTSTR file_path)
{
	return utils_file_exists(file_path);
}

BOOL sdc_utils_is_string_int(PCTSTR input)
{
	return utils_is_string_int(input);
}

BOOL sdc_utils_is_string_empty(PCTSTR input)
{
	return utils_is_string_empty(input);
}

TCHAR *sdc_utils_trim_left(TCHAR *input)
{
	return utils_trim_left(input);
}

TCHAR *sdc_utils_trim_right(TCHAR *input)
{
	return utils_trim_right(input);
}

TCHAR *sdc_utils_trim_both(TCHAR *input)
{
	return utils_trim_both(input);
}

TCHAR *sdc_utils_remove_trailing_enter(TCHAR *input)
{
	return utils_remove_trailing_enter(input);
}

int sdc_utils_find_char(TCHAR *input, TCHAR key)
{
	return utils_find_char(input, key);
}

BOOL sdc_utils_get_registry_value(PCTSTR key, PCTSTR entry, PTSTR value, DWORD valsize)
{
	return utils_get_registry_value(key, entry, value, valsize);
}

//======================================= Text Resources module

BOOL sdc_textres_enable(PCTSTR root_dir, PCTSTR filename)
{
	return text_res_enable(root_dir, filename);
}

PTSTR sdc_textres_get_value(PCTSTR key)
{
	return text_res_get_value(key);
}

BOOL sdc_textres_is_enabled()
{
	return text_res_is_enabled();
}

//======================================= Logger module

BOOL sdc_logger_enable(PCTSTR root_dir, PCTSTR filename)
{
	return logger_enable(root_dir, filename);
}

void sdc_logger_action(LOG_LEVEL log_level, LPCTSTR filename, int line, LPCTSTR format, ...)
{
	va_list args;
    va_start(args, format);
	logger_action(log_level, filename, line, format, args);
	va_end(args);
}

BOOL sdc_logger_is_enabled()
{
	return logger_is_enabled();
}

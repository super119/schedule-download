/*
 * Schedule Download Text reading module
 * Text strings will be read into a HashMap and used
 * by all threads.
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#include <map>
#include "..\include\textres.h"
#include "..\include\utils.h"
#include "..\include\logger.h"
#include "..\include\defs.h"
#include "..\include\macros.h"
#include <strsafe.h>
using namespace std;

// whether the textres is enabled
BOOL text_res_enabled = FALSE;

// text resource file path
TCHAR text_res_file_path[MAX_PATH];

// text resource file contents
LPVOID text_res_contents = NULL;

struct map_key_comp
{
	bool operator()(const TCHAR *a, const TCHAR *b) const
	{
		return _tcscmp(a, b) < 0;
	}
};

/*
 * Text resource container
 */
typedef map<const TCHAR *, TCHAR *, map_key_comp> TextResContainer;
TextResContainer text_res_container;

extern CRITICAL_SECTION sdc_cs_textres;
extern HANDLE sdc_heap;

static BOOL text_res_init()
{
	HANDLE hFile = NULL;
	HANDLE hHeap = NULL;
	LARGE_INTEGER file_size;
	DWORD n = 0;
	DWORD bytes_read = 0;
	TCHAR *token1 = NULL;
	TCHAR *token2 = NULL;
	TCHAR *next_token1 = NULL;
	TCHAR *next_token2 = NULL;
	// TextResContainer::iterator it = NULL;

	// Open the resource file
	hFile = CreateFile(text_res_file_path, GENERIC_READ, FILE_SHARE_READ, NULL, 
					OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile == INVALID_HANDLE_VALUE) {
		GET_ERROR_CODE(n);
		M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, 
			TEXT_RES_OPEN_FILE_FAILED, text_res_file_path, utils_format_error_string(n));
	}

	// Read the whole file into memory
	if (!GetFileSizeEx(hFile, &file_size)) {
		GET_ERROR_CODE(n);
		M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, 
			TEXT_RES_GET_FILE_SIZE_FAILED, text_res_file_path, utils_format_error_string(n));
	}
	// Append a \0 at the end of the buffer
	text_res_contents = HeapAlloc(sdc_heap, HEAP_ZERO_MEMORY, file_size.LowPart + sizeof(TCHAR));
	// HeapAlloc will not call SetLastError
	M_GTIF_WITH_LOG(text_res_contents != NULL, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, 
		TEXT_RES_HEAP_ALLOC_FAILED, file_size.LowPart);
	if (!ReadFile(hFile, text_res_contents, file_size.LowPart, &bytes_read, NULL) || 
		bytes_read > file_size.LowPart) {
		GET_ERROR_CODE(n);
		M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, TEXT_RES_READ_FILE_FAILED, 
			text_res_file_path, file_size.LowPart, bytes_read, utils_format_error_string(n));
	}
	LOGGER(LOG_LEVEL_INFO, __SDFILE__, __LINE__, TEXT_RES_READ_FILE_OK, text_res_file_path, file_size.LowPart, bytes_read);

	// check whether this is an Unicode-16 file(begin with 0xfffe)
	// UTF-8 file's BOM is 0xEFBBBF
	PUCHAR pTmp = (PUCHAR)text_res_contents;  // pTmp can't be PCHAR here because of 0xff/0xfe below
	if (pTmp[0] == 0xff && pTmp[1] == 0xfe) {
		pTmp += 2;
	} else {
		M_GTIF_WITH_LOG(FALSE, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, TEXT_RES_NOT_UNICODE_FILE, text_res_file_path);
	}
	// append \0
	*((TCHAR *)((PUCHAR)text_res_contents + bytes_read)) = (TCHAR)'\0'; 

	// read ok, split the string with delimiter \n first
	TCHAR delimiter1[] = TEXT("\n");
	TCHAR delimiter2[] = TEXT("=");
	token1 = _tcstok_s((TCHAR *)pTmp, delimiter1, &next_token1);
	// no delimiter1 found, this is illegal.
	// text resource file should at least contain a \n
	M_GTIF_WITH_LOG(token1 != NULL, failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, 
		TEXT_RES_FILE_ILLEGAL, text_res_file_path);
	for (; token1 != NULL; token1 = _tcstok_s(NULL, delimiter1, &next_token1)) {
		// remove the \r maybe exists at the end of the line
		token1 = utils_remove_trailing_enter(token1);
		M_CONT_IF_FAIL(token1 != NULL);
		// remove left & right spaces
		token1 = utils_trim_both(token1);
		M_CONT_IF_FAIL(token1 != NULL);
		// check whether this is a comment line
		if (*token1 == TCHAR('#')) continue;

		// ok, seperate by delimiter2 again to get the key & value pair
		token2 = _tcstok_s(token1, delimiter2, &next_token2);
		// if this line doesn't contain delimiter2, ignore...
		M_CONT_IF_FAIL(token2 != NULL);
		// remove the \r maybe exists at the end of the line
		token2 = utils_remove_trailing_enter(token2);
		M_CONT_IF_FAIL(token2 != NULL);
		// remove left & right spaces
		token2 = utils_trim_both(token2);
		M_CONT_IF_FAIL(token2 != NULL);
		// next_token2 is the value string after delimiter2
		next_token2 = utils_trim_left(next_token2);
		M_CONT_IF_FAIL(next_token2 != NULL);

		// ok, insert key & value into map
		pair<TextResContainer::iterator, bool> insert_result = 
				text_res_container.insert(make_pair(token2, next_token2));
		if (!insert_result.second) {
			// element already exists, update it
			insert_result.first->second = next_token2;
		}
	}

	// text resource load ok
	LOGGER(LOG_LEVEL_INFO, __SDFILE__, __LINE__, TEXT_RES_INIT_OK);

	// print all text resources -- just for debug
	/* it = text_res_container.begin();
	while (it != text_res_container.end()) {
		LOGGER(LOG_LEVEL_INFO, __SDFILE__, __LINE__, TEXT_RES_DEBUG_DUMP, it->first, it->second);
		it++;
	} */

	CloseHandle(hFile);
	hFile = NULL;
	return TRUE;

failed:
	if (hFile != NULL && hFile != INVALID_HANDLE_VALUE)
		CloseHandle(hFile);
	return FALSE;
}

BOOL text_res_enable(PCTSTR root_dir, PCTSTR filename)
{
	size_t root_dir_len = 0;

	EnterCriticalSection(&sdc_cs_textres);
	// logger enabled?
	M_GOTO_IF_FAIL(logger_is_enabled(), failed);
	// textres enabled?
	M_GOTO_IF_FAIL(text_res_enabled == FALSE, enabled);
	// parameter check
	M_GTIF_WITH_LOG(!utils_is_string_empty(root_dir), failed, LOG_LEVEL_ERROR, 
		__SDFILE__, __LINE__, TEXT_RES_ROOT_DIR_INPUT_ILLEGAL);
	M_GTIF_WITH_LOG(!utils_is_string_empty(filename), failed, LOG_LEVEL_ERROR, 
		__SDFILE__, __LINE__, TEXT_RES_FILENAME_INPUT_ILLEGAL);

	// generate text resource file path now
	M_GTIF_WITH_LOG(SUCCEEDED(StringCchLength(root_dir, MAX_PATH, &root_dir_len)), failed, 
		LOG_LEVEL_ERROR, __SDFILE__, __LINE__, TEXT_RES_GET_ROOT_DIR_LEN_FAILED, root_dir);
	if (root_dir[root_dir_len - 1] == (TCHAR)'\\') {
		M_GTIF_WITH_LOG(SUCCEEDED(StringCchPrintf(text_res_file_path, _countof(text_res_file_path), 
							TEXT("%s%s"), root_dir, filename)), failed, LOG_LEVEL_ERROR, 
							__SDFILE__, __LINE__, TEXT_RES_GEN_FILE_PATH_FAILED1, root_dir, filename);
	} else {
		M_GTIF_WITH_LOG(SUCCEEDED(StringCchPrintf(text_res_file_path, _countof(text_res_file_path), 
							TEXT("%s\\%s"), root_dir, filename)), failed, LOG_LEVEL_ERROR, 
							__SDFILE__, __LINE__, TEXT_RES_GEN_FILE_PATH_FAILED2, root_dir, filename);
	}

	// check whether this file exists
	M_GTIF_WITH_LOG(utils_file_exists(text_res_file_path), failed, LOG_LEVEL_ERROR, 
				__SDFILE__, __LINE__, TEXT_RES_FILE_NOT_FOUND, text_res_file_path); 

	LOGGER(LOG_LEVEL_INFO, __SDFILE__, __LINE__, TEXT_RES_ENABLE_OK, text_res_file_path);

	M_GTIF_WITH_LOG(text_res_init(), failed, LOG_LEVEL_ERROR, __SDFILE__, __LINE__, 
				TEXT_RES_INIT_FAILED, text_res_file_path);

	text_res_enabled = TRUE;
	LeaveCriticalSection(&sdc_cs_textres);
	return TRUE;
enabled:
	LeaveCriticalSection(&sdc_cs_textres);
	return TRUE;
failed:
	LeaveCriticalSection(&sdc_cs_textres);
	return FALSE;
}

PTSTR text_res_get_value(const TCHAR *key)
{
	M_RETURN_VAL_IF_FAIL(!utils_is_string_empty(key), NULL);
	M_RETURN_VAL_IF_FAIL(text_res_enabled, NULL);

	TextResContainer::iterator it = text_res_container.find(key);
	if (it != text_res_container.end())
		return it->second;

	return NULL;
}

BOOL text_res_is_enabled()
{
	return text_res_enabled;
}
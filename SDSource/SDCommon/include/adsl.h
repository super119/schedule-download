/*
 * Schedule Download ADSL utilities, based on windows RAS
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#ifndef _ADSL_H
#define _ADSL_H

#include <windows.h>
#include <tchar.h>
#include <Ras.h>
#include <RasError.h>

/*
 * Get all RAS entries in all phone book
 * RASENTRYNAME array is allocated on sdcommon's heap
 * @return: If no entries found, return NULL and *entry_num will be set to zero
 *			If error occurs, return NULL and *entry_num will be set to -1
 * MT Safe
 */
RASENTRYNAME *adsl_enum_entries(DWORD *entry_num);

/*
 * Query whether the specified entry has saved password
 * If user don't save the password for adsl entry
 * SDService can't dial this entry automatically
 * @return: 1 - saved password, 0 - not saved password, -1 - error occurs
 * MT Safe
 */
WORD adsl_entry_save_password(PCTSTR phone_book_path, PCTSTR entry_name);

/*
 * Dial the ADSL
 * @adsl_conn _out: Save the ADSL connection handle into this param if succeeded, or NULL if failed
 * MT Safe
 */
BOOL adsl_dial(PCTSTR phone_book_path, PCTSTR entry_name, HRASCONN *adsl_conn);

/*
 * Hangup active RAS connection
 * MT Safe
 */
BOOL adsl_hangup(HRASCONN adsl_conn);

#endif
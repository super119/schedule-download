/*
 * Schedule Download Http utilities, based on winhttp
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#ifndef _HTTP_H
#define _HTTP_H

#include <windows.h>
#include <tchar.h>
#include <winhttp.h>

/*
 * Check whether the internet is available by visiting the url specified
 * MT Safe
 */
BOOL http_check_internet(PCTSTR url);

/*
 * Connect and get http text data.
 * Normally, user should call check_data_len, recv, close after call this function.
 * @url must begin at "http://"
 * @req, @conn, @sess are all output params.
 * MT Safe
 */
BOOL http_get_text(PCTSTR url, HINTERNET *sess, HINTERNET *conn, HINTERNET *req);

/*
 * Connect and get http binary data. Normally, it's used to download stuffs.
 * @req, @conn, @sess are all output params.
 * MT Safe
 */
BOOL http_get_bin(PCTSTR url, HINTERNET *sess, HINTERNET *conn, HINTERNET *req);

/*
 * Check the response data length
 * Return TRUE if data is available and the @len saves the data length
 * Return TRUE if data isn't available and the @len will be set to 0
 * Return FALSE if error occurs
 * So check the return value and the @len both to make sure no error occurs
 * and the data length is not 0
 * MT Safe
 */
BOOL http_check_data_len(HINTERNET request, DWORD *len);

/*
 * Receive data from HTTP server.
 * MT Safe
 */
BOOL http_recv(HINTERNET request, LPVOID buffer, DWORD buffer_len, DWORD *bytes_read);

/*
 * Close the whole http session. Call http_send again to initialize another http session.
 * MT Safe
 */
void http_close(HINTERNET *session, HINTERNET *connect, HINTERNET *request);

#endif
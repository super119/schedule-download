/*
 * Schedule Download logger
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#ifndef _LOGGER_H
#define _LOGGER_H

#include <windows.h>
#include <tchar.h>

/* 
 * sdcommon.h has declared LOG_LEVEL enum, so we should not 
 * declare again here if sdcommon.h has been included
 */
#ifndef __SD_COMMON_H
enum LOG_LEVEL
{
	LOG_LEVEL_INFO,
	LOG_LEVEL_WARNING,
	LOG_LEVEL_ERROR
};
#endif

/*
 * Set log file root directory and filename
 * Logger will create a "log" directory under root dir, then create log file
 * under it, using the filename which specified.
 * CAUTION: call this function first to make logger available
 *          next call this function will be ignore and TRUE will be returned
 * MT safe
 */
BOOL logger_enable(PCTSTR root_dir, PCTSTR filename);

/*
 * Log the strings.
 * MT safe
 */
void logger_action(LOG_LEVEL log_level, LPCTSTR filename, 
										int line, LPCTSTR format, char *args);

/*
 * Log string. This is an internal function used only in SDCommon library
 * to provide the variable arguments
 */
void sdinternal_logger_action(LOG_LEVEL log_level, LPCTSTR filename, 
										int line, LPCTSTR format, ...);

/*
 * Check whether the logger is enabled
 * MT Safe
 */
BOOL logger_is_enabled();

#endif
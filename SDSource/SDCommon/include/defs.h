/*
 * Schedule Download Common Library
 * Used only inside SDCommon
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#ifndef _DEFS_H
#define _DEFS_H

#define ERROR_STRING_BUFSIZE				65536
#define MEGA_BYTES							1024 * 1024
#define REGISTRY_VALUE_SIZE					256

#define SD_REGISTRY_KEY						TEXT("Software\\nicolas.m.zhang@gmail.com\\sdconfig")
#define UTILS_ROOT_DIRECTORY_KEY_ENTRY		TEXT("RootDirectory")
#define UTILS_OPEN_REG_KEY_FAILED			TEXT("Open Registry key %s failed. Reason: %s")
#define UTILS_QUERY_REG_ENTRY_FAILED		TEXT("Query Registry entry %s failed. Reason: %s")

#define LOGGER_STRING_MAX					4096
#define LOGGER_MAX_ROTATE_NUM				10
#define LOGGER_INFO_SYNTAX					TEXT("INFO|%s:%d|%d-%d-%d %d:%d:%d|")
#define LOGGER_WARN_SYNTAX					TEXT("WARN|%s:%d|%d-%d-%d %d:%d:%d|")
#define LOGGER_ERROR_SYNTAX					TEXT("ERROR|%s:%d|%d-%d-%d %d:%d:%d|")

#define TEXT_RES_ROOT_DIR_INPUT_ILLEGAL		TEXT("Input parameter: root_dir is empty.")
#define TEXT_RES_FILENAME_INPUT_ILLEGAL		TEXT("Input parameter: filename is empty.")
#define TEXT_RES_GET_ROOT_DIR_LEN_FAILED	TEXT("Get root_dir's length failed. Root_dir is %s.")
#define TEXT_RES_GEN_FILE_PATH_FAILED1		TEXT("1st generate text resource file path failed. Root_dir is %s, filename is %s.")
#define TEXT_RES_GEN_FILE_PATH_FAILED2		TEXT("2nd generate text resource file path failed. Root_dir is %s, filename is %s.")
#define TEXT_RES_FILE_NOT_FOUND				TEXT("Cannot find text resource file: %s.")
#define TEXT_RES_ENABLE_OK					TEXT("TextRes enable OK. Text resource file is %s.")
#define TEXT_RES_INIT_FAILED				TEXT("TextRes init failed. TextRes file path is %s.")
#define TEXT_RES_OPEN_FILE_FAILED			TEXT("Open text resource file: %s failed. Reason: %s")
#define TEXT_RES_GET_FILE_SIZE_FAILED		TEXT("Get size of text resource file: %s failed. Reason: %s")
#define TEXT_RES_HEAP_ALLOC_FAILED			TEXT("Alloc memory for text resource file failed. Need memory size: %d")
#define TEXT_RES_READ_FILE_FAILED			TEXT("Read %s failed. Want reading bytes: %d, Actual read bytes: %d. Reason: %s")
#define TEXT_RES_READ_FILE_OK				TEXT("Read %s succeeded. File size: %d, read %d bytes.")
#define TEXT_RES_FILE_ILLEGAL				TEXT("Text resource file %s is illegal. At least should contain a \\n")
#define TEXT_RES_INIT_OK					TEXT("Text resource init success.")
#define TEXT_RES_DEBUG_DUMP					TEXT("Key %s, Value %s")
#define TEXT_RES_NOT_UNICODE_FILE			TEXT("Text resource file: %s is not an UTF-16 file.")

// all Timeouts -- 5 minutes
#define HTTP_RESOLVE_TO						300000
#define HTTP_CONNECT_TO						300000
#define HTTP_SEND_TO						300000
#define HTTP_RECV_TO						300000
#define HTTP_AGENT_NAME						TEXT("Schedule Downloader Service")
#define HTTP_PROXY_ENABLED_ENTRY			TEXT("ProxyEnabled")
#define HTTP_PROXY_SERVER_ENTRY				TEXT("ProxyServer")
#define HTTP_PROXY_PORT_ENTRY				TEXT("ProxyPort")
#define HTTP_PROXY_USER_ENTRY				TEXT("ProxyUser")
#define HTTP_PROXY_PASS_ENTRY				TEXT("ProxyPass")
#define HTTP_NO_PROXY_OPEN_FAILED			TEXT("WinHttpOpen failed. Reason: %s")
#define HTTP_PROXY_SERVER_ABSENT			TEXT("Http Proxy enabled, but the proxy server hasn't set.")
#define HTTP_PROXY_PORT_ABSENT				TEXT("Http Proxy enabled, but the proxy port hasn't set")
#define HTTP_PROXY_PORT_ILLEGAL				TEXT("Http proxy enabled, but the proxy port is not an integer.")
#define HTTP_STRING_OPERATION_FAILED		TEXT("Http StringCchXXX function operation failed.")
#define HTTP_OPEN_WITH_PROXY_FAILED			TEXT("Http open with proxy failed. Reason: %s")
#define HTTP_SET_PROXY_USER_FAILED			TEXT("Http set option -- proxy username failed. Reason: %s")
#define HTTP_SET_PROXY_PASS_FAILED			TEXT("Http set option -- proxy password failed. Reason: %s")
#define HTTP_SET_TIMEOUTS_FAILED			TEXT("Http set timeouts failed. Reason: %s")
#define HTTP_CONNECT_FAILED					TEXT("WinHttpConnect failed. Reason: %s")
#define HTTP_OPEN_REQUEST_FAILED			TEXT("WinHttpOpenRequest failed. Reason: %s")
#define HTTP_SEND_REQUEST_FAILED			TEXT("WinHttpSendRequest failed. Reason: %s")
#define HTTP_RECV_RESPONSE_FAILED			TEXT("WinHttpReceiveResponse failed. Reason: %s")
#define HTTP_QUERY_DATA_FAILED				TEXT("WinHttpQueryDataAvailable failed. Reason: %s")
#define HTTP_CRACK_URL_FAILED				TEXT("WinHttpCrackUrl failed. Reason: %s")
#define HTTP_CREATE_URL_GET_LEN_FAILED		TEXT("WinHttpCreateUrl try to get new url's length failed.")
#define HTTP_CREATE_URL_FAILED				TEXT("WinHttpCreateUrl failed. Reason: %s")
#define HTTP_HEAP_ALLOC_FAILED				TEXT("HeapAlloc in http module failed.")
#define HTTP_READ_DATA_FAILED				TEXT("WinHttpReadData failed. Reason: %s")

#define ADSL_RASAPI32_DLL					TEXT("rasapi32.dll")
#define ADSL_LOAD_DLL_FAILED				TEXT("Load rasapi32.dll failed. Reason: %s")
#define ADSL_GET_FN_ENUM_ENTRIES_FAILED		TEXT("Get RasEnumEntriesW function pointer failed. Reason: %s")
#define ADSL_HEAP_ALLOC_FAILED				TEXT("HeapAlloc failed. Alloc size is %d")
#define ADSL_ENUM_ENTRIES_FAILED			TEXT("Enum RAS entries failed. The final return code is %d")
#define ADSL_GET_FN_ENTRY_PARAMS_FAILED		TEXT("Get RasGetEntryDialParams function pointer failed. Reason: %s")
#define ADSL_STRING_OPERATE_FAILED			TEXT("ADSL module StringCchXXX operation failed.")
#define ADSL_GET_ENTRY_DIAL_PARAM_FAILED	TEXT("RasGetEntryDialParams failed. The return code is %d")
#define ADSL_DIAL_NOT_SAVE_PASS				TEXT("RasDial can't dial a non-saved password entry.")
#define ADSL_DIAL_FAILED					TEXT("RasDial failed. The return code is %d")
#define ADSL_DIAL_CONN_NULL					TEXT("RasDial success but the HRASCONN is NULL.")
#define ADSL_GET_FN_DIAL_FAILED				TEXT("Get RasDial function pointer failed. Reason: %s")
#define ADSL_GET_FN_HANGUP_FAILED			TEXT("Get RasHangUp function pointer failed. Reason: %s")
#define ADSL_HANGUP_FAILED					TEXT("RasHangUp failed. The return code is %d")

#endif
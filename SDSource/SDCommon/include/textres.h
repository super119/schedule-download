/*
 * Schedule Download Text reading module
 * Text strings will be read into a HashMap and used
 * by all threads.
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#ifndef _TEXT_RES_H
#define _TEXT_RES_H

#include <windows.h>
#include <tchar.h>

/*
 * Enable TextRes module. Set root directory and the text
 * resource file name to achieve it.
 * TextRes will check whether the logger is enabled first
 * @return: TRUE if success, FALSE if failed, check the log file
 *          to see the reason. If log file is empty, this is because logger
 *          is not enabled
 * MT Safe
 */
BOOL text_res_enable(PCTSTR root_dir, PCTSTR filename);

/*
 * Check out the text string by specified key
 * MT Safe
 */
PTSTR text_res_get_value(PCTSTR key);

/*
 * Check whether text resource module is enabled
 * MT Safe
 */
BOOL text_res_is_enabled();


#endif
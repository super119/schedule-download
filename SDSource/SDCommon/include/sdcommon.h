/*
 * Schedule Download SDCommon header file
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#ifndef __SD_COMMON_H
#define __SD_COMMON_H

#include <windows.h>
#include <tchar.h>
#include <Ras.h>
#include <RasError.h>
#include <winhttp.h>

#ifdef SDCOMMONAPI
#else
#define SDCOMMONAPI extern "C" __declspec(dllimport)
#endif

//======================================= Http module

SDCOMMONAPI BOOL sdc_http_check_internet(PCTSTR url);
SDCOMMONAPI BOOL sdc_http_get_text(PCTSTR url, HINTERNET *sess, HINTERNET *conn, HINTERNET *req);
SDCOMMONAPI BOOL sdc_http_get_bin(PCTSTR url, HINTERNET *sess, HINTERNET *conn, HINTERNET *req);
SDCOMMONAPI BOOL sdc_http_check_data_len(HINTERNET request, DWORD *len);
SDCOMMONAPI BOOL sdc_http_recv(HINTERNET request, LPVOID buffer, DWORD buffer_len, DWORD *bytes_read);
SDCOMMONAPI void sdc_http_close(HINTERNET *session, HINTERNET *connect, HINTERNET *request);

//======================================= Adsl module

SDCOMMONAPI RASENTRYNAME *sdc_adsl_enum_entries(DWORD *entry_num);
SDCOMMONAPI WORD sdc_adsl_entry_save_password(PCTSTR phone_book_path, PCTSTR entry_name);
SDCOMMONAPI BOOL sdc_adsl_dial(PCTSTR phone_book_path, PCTSTR entry_name, HRASCONN *adsl_conn);
SDCOMMONAPI BOOL sdc_adsl_hangup(HRASCONN adsl_conn);

//======================================= Utility module

SDCOMMONAPI PTSTR sdc_utils_format_error_string(const LONG code);
SDCOMMONAPI BOOL sdc_utils_find_root_directory(PTSTR root_dir);
SDCOMMONAPI BOOL sdc_utils_file_exists(PCTSTR file_path);
SDCOMMONAPI BOOL sdc_utils_is_string_int(PCTSTR input);
SDCOMMONAPI BOOL sdc_utils_is_string_empty(PCTSTR input);
SDCOMMONAPI TCHAR *sdc_utils_trim_left(TCHAR *input);
SDCOMMONAPI TCHAR *sdc_utils_trim_right(TCHAR *input);
SDCOMMONAPI TCHAR *sdc_utils_trim_both(TCHAR *input);
SDCOMMONAPI TCHAR *sdc_utils_remove_trailing_enter(TCHAR *input);
SDCOMMONAPI int sdc_utils_find_char(TCHAR *input, TCHAR key);
SDCOMMONAPI BOOL sdc_utils_get_registry_value(PCTSTR key, PCTSTR entry, PTSTR value, DWORD valsize);

//======================================= Text Resources module

#define GETTEXT(s)	sdc_textres_get_value(TEXT(s))

SDCOMMONAPI BOOL sdc_textres_enable(PCTSTR root_dir, PCTSTR filename);
SDCOMMONAPI PTSTR sdc_textres_get_value(PCTSTR key);
SDCOMMONAPI BOOL sdc_textres_is_enabled();

//======================================= Logger module

enum LOG_LEVEL
{
	LOG_LEVEL_INFO,
	LOG_LEVEL_WARNING,
	LOG_LEVEL_ERROR
};

SDCOMMONAPI BOOL sdc_logger_enable(PCTSTR root_dir, PCTSTR filename);
SDCOMMONAPI void sdc_logger_action(LOG_LEVEL log_level, LPCTSTR filename, int line, LPCTSTR format, ...);
SDCOMMONAPI BOOL sdc_logger_is_enabled();



#endif
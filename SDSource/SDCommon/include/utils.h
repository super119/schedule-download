/*
 * Schedule Download Utilities
 * Written by Eric Zhang <nicolas.m.zhang@gmail.com>
 */

#ifndef _UTILS_H
#define _UTILS_H

#include <windows.h>
#include <tchar.h>

/* 
 * Convert system error code to error message string
 * TLS used inside to make sure all threads have a local storage
 * to save the error string
 * @return: NULL if failed
 * MT Safe
 */
PTSTR utils_format_error_string(const LONG code);

/*
 * Find out program root directory
 * First looks up the registry, if failed, use current working directory
 * Call GetLastError to get error number if failed
 * MT Safe
 */
BOOL utils_find_root_directory(PTSTR root_dir);

/*
 * See whether a file exists by calling GetFileAttributes
 * MT Safe
 */
BOOL utils_file_exists(PCTSTR file_path);

/*
 * Check whether the input string is an integer
 * MT Safe
 */
BOOL utils_is_string_int(PCTSTR input);

/*
 * See whether a string is an empty string
 * Empty string is @input is NULL or just contain white spaces
 * MT Safe
 */
BOOL utils_is_string_empty(PCTSTR input);

/*
 * Trim the leading spaces in the string
 * @return: NULL if failed. The trimed string's start position if success.
 * MT Safe
 */
TCHAR *utils_trim_left(TCHAR *input);

/*
 * Trim the trailing spaces in the string
 * @return: NULL if failed. If success, the return pointer is the same
 *			as the input pointer because function modifies the trailing part of string
 * MT Safe
 */
TCHAR *utils_trim_right(TCHAR *input);

/*
 * Trim the left & right spaces of string
 * @return: NULL if failed.
 * MT Safe
 */
TCHAR *utils_trim_both(TCHAR *input);

/*
 * Remove the trailing enter(\r)
 * @return: NULL if failed. If success, the return pointer is the same
 *			as the input pointer because function modifies the trailing part of string
 * MT Safe
 */
TCHAR *utils_remove_trailing_enter(TCHAR *input);

/*
 * Find first occurance of @key char in @input string
 * Return index, begins at 0
 * Return -1 if not found, -2 if error occurs
 * MT Safe
 */
int utils_find_char(TCHAR *input, TCHAR key);

/* 
 * Get registry value specified by key & entry
 * @value points to the buffer receives the value
 * @valsize means the buffer size of @value, in bytes
 *  and the actual bytes has been written is also saved in it
 * MT Safe
 */
BOOL utils_get_registry_value(PCTSTR key, PCTSTR entry, PTSTR value, DWORD valsize);


#endif